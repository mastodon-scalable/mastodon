package NettyHTTP;

import QueueClients.*;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.*;
import io.netty.util.CharsetUtil;
import org.json.JSONException;
import org.json.JSONObject;


import java.util.Arrays;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static io.netty.handler.codec.http.HttpResponseStatus.CONTINUE;
import static io.netty.handler.codec.http.HttpVersion.HTTP_1_1;

public class HTTPHandler extends SimpleChannelInboundHandler<Object> {
    private HttpRequest request;
    private StringBuilder requestBody = new StringBuilder();
    private final static HashMap<String, Class> clients = new HashMap<>();

    static {
        clients.put("user", UserClient.class);
        clients.put("instance", InstanceClient.class);
        clients.put("notifications", NotificationsClient.class);
        clients.put("chat", ChatClient.class);
        clients.put("toots", TootsClient.class);
    }

    ExecutorService executorService = Executors.newCachedThreadPool();


    public void channelReadComplete(ChannelHandlerContext ctx) {
        ctx.flush();
        ctx.fireChannelReadComplete();
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, Object msg) throws Exception {
        if (msg instanceof HttpRequest) {
            HttpRequest request = this.request = (HttpRequest) msg;
            if (HttpUtil.is100ContinueExpected(request)) {
                send100Continue(ctx);
            }

        }
        if (msg instanceof HttpContent) {
            HttpContent httpContent = (HttpContent) msg;
            requestBody.append(httpContent.content().toString(CharsetUtil.UTF_8));
            if (msg instanceof LastHttpContent) {
                LastHttpContent trailer = (LastHttpContent) msg;

                String responseBody;
                String method = request.method().toString();
                String uri = request.uri();

                String service = (request.uri().split("/")[1]);
                if (!clients.containsKey(service))
                    responseBody = "{\"status\":\"404\", \n\"message\":\"uri not found\"}";
                else
                    responseBody = ((Client) clients.get(service).newInstance()).call(method + '|' + uri + '|' + requestBody.toString());


                int status_code;
                try{
                    JSONObject response = new JSONObject(responseBody);
                    status_code = Integer.parseInt(response.getString("status"));
                }catch (JSONException je){
                    status_code = 500;
                }

                FullHttpResponse response = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.valueOf(status_code), Unpooled.wrappedBuffer(responseBody.getBytes()));

                response.headers().set("Content-Type", "application/json");
                ChannelFuture future = ctx.writeAndFlush(response);
                future.addListener(ChannelFutureListener.CLOSE);
                System.out.println(response.toString());
            }
        }


    }


    private static void send100Continue(ChannelHandlerContext ctx) {
        FullHttpResponse response = new DefaultFullHttpResponse(HTTP_1_1,
                CONTINUE);
        ctx.writeAndFlush(response);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        cause.printStackTrace();
        ctx.close();
    }

}

