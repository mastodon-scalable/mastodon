package QueueClients;

import java.io.IOException;

public interface Client {
    String call(String message) throws IOException, InterruptedException;
}
