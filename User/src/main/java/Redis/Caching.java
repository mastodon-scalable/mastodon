package Redis;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

public class Caching {

    private static JedisPool jedisPool = new JedisPool(new JedisPoolConfig(), "localhost", 6379);

    public static void cacheUser(String name , String password){
        try (Jedis jedis = jedisPool.getResource()) {
            jedis.set(name, password);
        }
    }
    public static String checkLogin(String name){
        try (Jedis jedis = jedisPool.getResource()) {
            String re = jedis.get(name);
            return re;
        }
    }
}
