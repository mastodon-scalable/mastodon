import CommandPatterns.*;
import org.json.JSONObject;

import java.sql.Connection;
import java.sql.SQLException;


public class RequestHandler {
    String message;
    final Connection connection;

    public RequestHandler(String msg, Connection c)
    {
        this.message = msg;
        this.connection = c;
    }


    public String run() throws SQLException {
        String response = "";
        String[] messageSplit = this.message.split("\\|");
        String []  function = messageSplit[1].split("/");
        JSONObject json = new JSONObject(messageSplit[2]);
        String functionType = function[2];
        //You have to send a token with every request which is the username of the
        // user who wants to execute the command
        switch (functionType){
            case "GetAllUsers":
                try{
                    GetAllUsers getAllUsers1  = new GetAllUsers();
                    response += getAllUsers1.execute();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case "SignUp":
                try{
                    User user = new User(json);
                    SignUp signup = new SignUp(user);
                    response+= signup.execute();
                } catch (SQLException throwable) {
                    throwable.printStackTrace();
                }
                break;
            case "Login"   :
                try{
                    Login login = new Login(json.getString("name"), json.getString("password"),connection);
                    response+= login.execute();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case "FollowUser"  :
                try{
                    FollowUser followuser = new FollowUser
                            (json.getInt("follower_id"),
                            json.getInt("followed_id"),
                                    json.getString("token"));
                    response+= followuser.execute();
                } catch (SQLException throwable) {
                    throwable.printStackTrace();
                }
                break;
            case "UnFollowUser"  :
                try{
                    UnFollowUser unfollowuser = new UnFollowUser
                            (json.getInt("follower_id"),
                                    json.getInt("followed_id"),
                                    json.getString("token"));
                    response+= unfollowuser.execute();
                } catch (SQLException throwable) {
                    throwable.printStackTrace();
                }
                break;
            case "BlockUser"  :
                try{
                    BlockUser blockuser = new BlockUser
                            (json.getInt("user_id"),
                                    json.getInt("blocked_id"),
                                    json.getString("token"));
                    response+= blockuser.execute();
                } catch (SQLException throwable) {
                    throwable.printStackTrace();
                }
                break;
            case "ReportUser"  :
                try{
                    ReportUser reportuser = new ReportUser
                            (json.getInt("user_id"),
                                    json.getInt("reported_id"),
                                    json.getString("text"),
                                    json.getString("token"));
                    response+= reportuser.execute();
                } catch (SQLException throwable) {
                    throwable.printStackTrace();
                }
                break;
            case "DeleteAccount"  :
                try{
                    DeleteAccount deleteaccount = new DeleteAccount
                            (json.getString("username"),
                                    json.getString("password"),
                                    json.getString("token"));
                    response+= deleteaccount.execute();
                } catch (SQLException throwable) {
                    throwable.printStackTrace();
                }
                break;
            case "UpdateProfile"   :
                try{
                    UpdateProfile updateProfile = new UpdateProfile(json.getString("username"),
                            json.getString("password"),
                            json.getString("email"),
                            json.getInt("user_id"));
                    response+= updateProfile.execute();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
        return response;
      }
    }

