package CommandPatterns;

import Redis.Caching;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;


import java.sql.*;


public class Login implements Command{
    String name;
    String password;
    Connection connection;

    public Login(String name, String password, Connection c){
        this.name = name;
        this.password = password;
        this.connection = c;
    }


    @Override
    public String execute() throws SQLException {

        try{
            String response = "";
            String connectionUrl = "jdbc:mysql://localhost:3306/mastodon?serverTimezone=UTC";
            Connection conn = DriverManager.getConnection(connectionUrl, "root", "");
            CallableStatement stmt=conn.prepareCall("{call Login(?,?,?)}");
            stmt.setString(1,this.name);
            stmt.setString(2,this.password);
            stmt.registerOutParameter(3,Types.INTEGER);
            stmt.execute();
            int valid = stmt.getInt(3);
            System.out.println(valid);
            if (valid > 0){
                response += "Logged in successfully#"+this.name+"#"+this.password;
                Caching.cacheUser(this.name, this.password);
            }



            return  response;



        }
        catch (Exception e){
            return e.toString();
        }
    }
}
