package CommandPatterns;

import Redis.Caching;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class UpdateProfile implements Command{
    String name;
    String password;
    String email;
    int user_id;

    public UpdateProfile(String name, String password, String email,int user_id){
        this.name = name;
        this.password = password;
        this.email = email;
        this.user_id = user_id;
    }
    @Override
    public String execute() throws SQLException {
        String check = Caching.checkLogin(this.name);
        if(check == null){
            String response = "Try to login and try again :(";
            return response;
        }
        else{
            try{
                String connectionUrl = "jdbc:mysql://localhost:3306/mastodon?serverTimezone=UTC";
                Connection conn = DriverManager.getConnection(connectionUrl, "root", "");
                CallableStatement stmt=conn.prepareCall("{call update_profile(?,?,?,?)}");
                stmt.setString(1,this.name);
                stmt.setString(2,this.password);
                stmt.setString(3,this.email);
                stmt.setInt(4,this.user_id);
                stmt.execute();
                conn.close();
                return "user updated successfully";
            }
            catch (Exception e){
                return e.toString();
            }
        }
    }
}
