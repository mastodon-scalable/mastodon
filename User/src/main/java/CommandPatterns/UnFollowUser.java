package CommandPatterns;

import Redis.Caching;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class UnFollowUser implements Command{

    int follower_id;
    int followed_id;
    String token;

    public UnFollowUser(int follower_id, int followed_id,String token){
        this.followed_id = followed_id;
        this.follower_id = follower_id;
        this.token = token;
    }

    @Override
    public String execute() throws SQLException {
        String check = Caching.checkLogin(this.token);
        if(check == null){
            String response = "Try to login and try again :(";
            return response;
        }
        else{
            try{
                String response = "";
                String connectionUrl = "jdbc:mysql://localhost:3306/mastodon?serverTimezone=UTC";
                Connection conn = DriverManager.getConnection(connectionUrl, "root", "");
                CallableStatement stmt=conn.prepareCall("{call unfollow_user(?,?)}");
                stmt.setInt(1,this.follower_id);
                stmt.setInt(2,this.followed_id);
                stmt.execute();
                response += "you unfollowed him successfully";
                return response;
            }catch (Exception e){
                return e.toString();
            }
        }
    }
}
