package CommandPatterns;

import java.sql.SQLException;

public interface Command {


        public String execute() throws SQLException;



}
