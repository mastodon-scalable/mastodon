package CommandPatterns;

import java.sql.*;

public class GetAllUsers implements Command {


    @Override
    public  String execute() {
        String results = "";
        String sqlString = "SELECT * FROM users";
        String connectionUrl = "jdbc:mysql://localhost:3306/mastodon?serverTimezone=UTC";
        try (Connection conn = DriverManager.getConnection(connectionUrl, "root", "");
             PreparedStatement ps = conn.prepareStatement(sqlString);
             ResultSet rs = ps.executeQuery()) {
             while (rs.next()) {
                String name = rs.getString("name");
                String email = rs.getString("email");
                results+= "name"+':'+name +","+
                        "email"+':'+ email+"\n";
            }
            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException e) {
            // handle the exception
        }
        return results;
    }
}
