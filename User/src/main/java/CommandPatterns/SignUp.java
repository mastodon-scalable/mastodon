package CommandPatterns;

import java.sql.*;

public class SignUp implements Command {
    User user;

    public SignUp(User user) {
        this.user = user;
    }

    @Override
    public String execute() throws SQLException {
        try{
            String connectionUrl = "jdbc:mysql://localhost:3306/mastodon?serverTimezone=UTC";
            Connection conn = DriverManager.getConnection(connectionUrl, "root", "");
            CallableStatement stmt=conn.prepareCall("{call sign_up(?,?,?,?,?,null,null)}");
            stmt.setInt(1,user.instance_id);
            stmt.setString(2,user.name);
            stmt.setString(3,user.handle);
            stmt.setString(4,user.password);
            stmt.setString(5,user.email);
            stmt.execute();
            conn.close();
            return "user added successfully";
        }


        catch (Exception e){
            return e.toString();
        }

    }
}