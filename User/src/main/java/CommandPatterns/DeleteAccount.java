package CommandPatterns;

import Redis.Caching;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DeleteAccount implements Command{
    String username;
    String password;
    String token;

    public DeleteAccount(String username, String password,String token){
        this.username = username;
        this.password = password;
        this.token = token;
    }

    @Override
    public String execute() throws SQLException {
        String check = Caching.checkLogin(this.token);
        if(check == null){
            String response = "Try to login and try again :(";
            return response;
        }
        else{
            try{
                String response = "";
                String connectionUrl = "jdbc:mysql://localhost:3306/mastodon?serverTimezone=UTC";
                Connection conn = DriverManager.getConnection(connectionUrl, "root", "");
                CallableStatement stmt=conn.prepareCall("{call delete_account(?,?)}");
                stmt.setString(1,this.username);
                stmt.setString(2,this.password);
                stmt.execute();
                response += "You deleted your account, Sorry to see you go ";
                return response;
            }catch (Exception e){
                return e.toString();
            }
        }
    }
}
