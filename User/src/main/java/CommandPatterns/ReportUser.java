package CommandPatterns;

import Redis.Caching;
import com.google.common.cache.Cache;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ReportUser implements Command{
    int user_id;
    int blocked_id;
    String text;
    String token;

    public ReportUser(int user_id, int blocked_id, String text,String token){
        this.user_id = user_id;
        this.blocked_id = blocked_id;
        this.text = text;
        this.token = token;
    }
    @Override
    public String execute() throws SQLException {
        String check = Caching.checkLogin(this.token);
        if(check == null){
            String response = "Try to login and try again :(";
            return response;
        }
        else{
            try{
                String response = "";
                String connectionUrl = "jdbc:mysql://localhost:3306/mastodon?serverTimezone=UTC";
                Connection conn = DriverManager.getConnection(connectionUrl, "root", "");
                CallableStatement stmt=conn.prepareCall("{call report_user(?,?,?)}");
                stmt.setInt(1,this.user_id);
                stmt.setInt(2,this.blocked_id);
                stmt.setString(3, this.text);
                stmt.execute();
                response += "you reported him successfully";
                return response;
            }catch (Exception e){
                return e.toString();
            }
        }
    }
}
