package CommandPatterns;
import org.json.JSONObject;

import javax.swing.*;

public class User {
    String name;
    String email;
    String password;
    String handle;
    int instance_id;
    ImageIcon profile_photo;
    ImageIcon cover_photo;

    public User(JSONObject body){
        this.instance_id = body.getInt("instance_id");
        this.name = body.getString("name");
        this.email = body.getString("email");
        this.password = body.getString("password");
        this.handle = body.getString("handle");
    }


}
