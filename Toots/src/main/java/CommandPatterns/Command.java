package CommandPatterns;
import org.json.JSONObject;

public interface Command {

    public String execute(JSONObject json);
}
