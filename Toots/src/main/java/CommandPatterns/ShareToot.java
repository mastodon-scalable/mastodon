package CommandPatterns;
import com.mongodb.*;
import org.json.JSONObject;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
public class ShareToot implements Command {

    static MongoClient mongoClient =  new MongoClient(new MongoClientURI("mongodb://localhost:27017"));;


    public String execute(JSONObject json)
    {
        DB database = mongoClient.getDB("TheDatabaseName");
        DBCollection collection = database.getCollection("TheCollectionName");

        DBObject person = new BasicDBObject("userid", json.get("userid"))
                .append("tootid", json.get("tootid"));


        collection.insert(person);

        JSONObject returnstatus = new JSONObject();
        returnstatus.put("status", "200 ok");
        return returnstatus.toString();
    }
}