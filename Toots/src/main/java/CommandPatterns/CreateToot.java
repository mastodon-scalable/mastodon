package CommandPatterns;
import com.mongodb.*;
import org.json.JSONObject;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
public class CreateToot implements Command {
//    Toot toot;
    static MongoClient mongoClient =  new MongoClient(new MongoClientURI("mongodb://localhost:27017"));

//    public CreateToot(Toot t, Connection c){
//        toot =t;
//        connection = c;
//    }

    public String execute(JSONObject json)
    {
        DB database = mongoClient.getDB("Mastodon");
        DBCollection collection = database.getCollection("Toots");


        DBObject person = new BasicDBObject("userid", json.get("userid"))
                .append("tootString", json.get("tootString"));

        collection.insert(person);

        JSONObject returnstatus = new JSONObject();
        returnstatus.put("status", "200 ok");
        return returnstatus.toString();
    }

    public static void main(String[] args) {
        CreateToot ct = new CreateToot();
        JSONObject testobj = new JSONObject();
        testobj.put("userid", "01");
        testobj.put("tootString", "test toot string");

        ct.execute(testobj);
    }
}
