package CommandPatterns;
import com.mongodb.*;
import com.mongodb.util.JSON;
import org.json.JSONObject;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
public class GetUserToots implements Command {
    static MongoClient mongoClient =  new MongoClient(new MongoClientURI("mongodb://localhost:27017"));;


//    public GetUserToots(Toot t, Connection c){
//        toot =t;
//        connection = c;
//    }

    public String execute(JSONObject json)
    {
        DB database = mongoClient.getDB("Mastodon");
        DBCollection collection = database.getCollection("toots");

        DBObject query = new BasicDBObject("userid", json.get("userid"));
        DBCursor cursor = collection.find(query);

        JSONObject result = new JSONObject();
        int counter = 0;
        while (cursor.hasNext()) {
            result.append(counter+"", new JSONObject(new JSON().serialize(cursor.next())).toString());
            counter++;
        }

        return result.toString();
    }

    public static void main(String[] args) {
        GetUserToots gt = new GetUserToots();

        JSONObject name = new JSONObject();
        name.put("userid", "01");
        String userToots = gt.execute(name);
        System.out.println(userToots);
    }

}