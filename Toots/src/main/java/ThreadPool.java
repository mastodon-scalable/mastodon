import CommandPatterns.*;
import CommandPatterns.Command;
import com.rabbitmq.client.*;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class ThreadPool {
    private static ExecutorService threadPool = Executors.newFixedThreadPool(10);


    //Singleton Design Pattern
    public static ExecutorService getThreadPool(int threads) {
        if (threadPool.equals(null)){
            threadPool = Executors.newFixedThreadPool(threads);
        }
        return threadPool;
    }

    public static void main(String[] args) {
        ConcurrentHashMap<String, Class> hm = new ConcurrentHashMap();

        hm.put("CreateToot", CreateToot.class);
        hm.put("GetUserToots", GetUserToots.class);
        hm.put("ReshareToot", ReshareToot.class);
        hm.put("ShareToot", ShareToot.class);
        hm.put("ToggleLikeToot", ToggleLikeToot.class);

        String RECEIVE_QUEUE_NAME = "tootsQueueReceive";
        String RESPONSE_QUEUE_NAME = "tootsQueueResponse";
        //Core logic of application goes here

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection =null;
        Channel channel = null;
        try {
            connection = factory.newConnection();

            channel = connection.createChannel();

        }catch(Exception e) { e.printStackTrace(); }

        try{
            channel.queueDeclare(RECEIVE_QUEUE_NAME, false, false, false, null);
        }
        catch (Exception e) { e.printStackTrace();}


        Channel finalChannel = channel;
        Consumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
                    throws IOException {
                String message = new String(body, "UTF-8");
                System.out.println(" [x] Received '" + message + "'");

                Runnable task = new Runnable() {
                    public void run() {
                        JSONObject rcvj = new JSONObject(message);
                        try {
                            Command obj = (Command) hm.get(rcvj.get("command")).newInstance( );
                            String result = (String) hm.get(rcvj.get("command")).getMethod("execute", JSONObject.class).invoke(obj, rcvj);

//                            Channel channel= finalConnection.createChannel();
                            finalChannel.queueDeclare(RESPONSE_QUEUE_NAME, false, false, false, null);

                            finalChannel.basicPublish("", RESPONSE_QUEUE_NAME, null, result.getBytes("UTF-8"));
                            System.out.println("PUBLISHED "+result);

                        }
                        catch(Exception e)
                        { e.printStackTrace(); }

                    }
                };

                getThreadPool(10).submit(task);
                System.out.println("SUBMITTED");
            }
        };

        try {
            System.out.println("HANDLING");
            String rcvJSON = channel.basicConsume(RECEIVE_QUEUE_NAME, true, consumer);


            Connection finalConnection = connection;



        }
        catch(Exception e) {
            e.printStackTrace();
        }

    }




}
