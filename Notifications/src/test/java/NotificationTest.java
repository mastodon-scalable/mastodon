

import org.json.JSONObject;
import org.junit.*;

import java.sql.*;
import java.time.LocalDate;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


public class NotificationTest {

    static Connection c;//password for db
    static final String DBName = System.getenv("Mastodon_DB_N");
    static final String Password = System.getenv("Mastodon_DB_Pass");
    static final String Port = System.getenv("Mastodon_DB_P");
    static final String Host = System.getenv("Mastodon_DB_H");
    static final String User = System.getenv("Mastodon_DB_U");
    static {
        try {
            c = DriverManager
                    .getConnection("jdbc:postgresql://"+Host+':'+Port+'/'+DBName,
                            User, Password);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    @BeforeClass
    public static void createUser()
    {
        PreparedStatement stmt1;
        PreparedStatement stmt2;
        try {
        String sql = "INSERT INTO _users ( name, handle, password, email, profile_photo, cover_photo) VALUES (?,?,?,?,'','');";
            stmt1 = c.prepareStatement(sql);
        stmt1.setString(1, "mostafa abdelaziz");
        stmt1.setString(2, "mosta");
        stmt1.setInt(3, 12345);
        stmt1.setString(4, "mosta@mosta.com");
        stmt1.executeUpdate();
        stmt1.close();

        String sql2 = "INSERT INTO _users ( name, handle, password, email, profile_photo, cover_photo) VALUES (?,?,?,?,'','');";
        stmt2 = c.prepareStatement(sql2);
        stmt2.setString(1, "mohsen eldemerdash");
        stmt2.setString(2, "mohseeeeen");
        stmt2.setInt(3, 123456);
        stmt2.setString(4, "mohesn@mohsen.com");
        stmt2.executeUpdate();
        stmt2.close();
        }
        catch (SQLException throwables) {
        throwables.printStackTrace();
    }
    }

    @AfterClass
    public static void deleteUser()
    {
        PreparedStatement stmt;
        try {
            String sql = "DELETE FROM _users WHERE handle=? and handle=?;";
            stmt = c.prepareStatement(sql);
            stmt.setString(1, "mosta");
            stmt.setString(2, "mohseeeeen");
            stmt.executeUpdate();
            stmt.close();
        }
        catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    @Test
    public void SetReadSuccess() {
        String msg = "PUT|/notifications/set-read|{\"id\":\"1\"}";
        RequestHandler handler = new RequestHandler(msg, c);
        JSONObject response = new JSONObject(handler.run());
        assertEquals(201, Integer.parseInt((String) response.get("status")));
    }
    /**
     *  succeeds when the actor,
     *  and the notified users are found and the attributes are persent in the JSON object.
     */
    @Test
    public void CreateNotificationSuccess() {
        String msg = "POST|/notifications|{" +
                "\"notified_id\":\"1\",\n" +
                " \"actor_id\":\"2\",\n" +
                " \"text\":\"ttestttextttt\",\n" +
                " \"type\":\"T\",\n"+
                "}";
        RequestHandler handler = new RequestHandler(msg, c);
        JSONObject response = new JSONObject(handler.run());
        assertEquals(201, Integer.parseInt((String) response.get("status")));
    }
    /**
     *  fails when the object is missing one of the attributes
     */
    @Test
    public void CreateNotificationFail() {
        String msg = "POST|/notifications|{" +
                "\"notified_id\":\"1\",\n" +
                " \"actor_id\":\"2\",\n" +
                " \"type\":\"Tdd\",\n"+
                "}";
        RequestHandler handler = new RequestHandler(msg, c);
        JSONObject response = new JSONObject(handler.run());
        assertEquals(404, Integer.parseInt((String) response.get("status")));
    }
    /**
     *  fails when typing a notifier that is not found
     */
    @Test
    public void CreateNotificationFailNotifiedNotFound() {
        String msg = "POST|/notifications|{" +
                "\"notified_id\":\"55\",\n" +
                " \"actor_id\":\"2\",\n" +
                " \"text\":\"ttestttextttt\",\n" +
                " \"type\":\"Tdd\",\n"+
                "}";
        RequestHandler handler = new RequestHandler(msg, c);
        JSONObject response = new JSONObject(handler.run());
        assertEquals(501, Integer.parseInt((String) response.get("status")));
    }
    /**
     *  fails when typing an actor that is not found
     */
    @Test
    public void CreateNotificationFailActorNotFound() {
        String msg = "POST|/notifications|{" +
                "\"notified_id\":\"1\",\n" +
                " \"actor_id\":\"51\",\n" +
                " \"text\":\"ttestttextttt\",\n" +
                " \"type\":\"Tdd\",\n"+
                "}";
        RequestHandler handler = new RequestHandler(msg, c);
        JSONObject response = new JSONObject(handler.run());
        assertEquals(501, Integer.parseInt((String) response.get("status")));
    }
    /**
     *  succeeds
     */
    @Before
    public void CreateNotifications(){
        PreparedStatement stmt;
        try
        {
            String sql = "INSERT INTO public._notifications (actor_id, notified_id, type, read, date) VALUES (?,?,?,?,?);";
            stmt = c.prepareStatement(sql);
            stmt.setInt(1, 1);
            stmt.setInt(2, 2);
            stmt.setString(3, "like");
            stmt.setInt(4, 0);
            stmt.setString(5, LocalDate.now().toString());
            stmt.executeUpdate();
            stmt.close();
            sql = "INSERT INTO public._notifications (actor_id, notified_id, type, read, date) VALUES (?,?,?,?,?);";
            stmt = c.prepareStatement(sql);
            stmt.setInt(1, 2);
            stmt.setInt(2, 1);
            stmt.setString(3, "follow");
            stmt.setInt(4, 0);
            stmt.setString(5, LocalDate.now().toString());
            stmt.executeUpdate();
            stmt.close();
            stmt.close();
        }
        catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
    @After
    public void DeleteNotifications(){
        PreparedStatement stmt;
        try
        {
            String sql = "DELETE FROM public._notifications WHERE actor_id=? and actor_id=?";
            stmt = c.prepareStatement(sql);
            stmt.setInt(1, 2);
            stmt.setInt(2, 1);
        }
        catch (SQLException throwables)
        {
            throwables.printStackTrace();
        }
    }
    @Test
    public void GetNotficationSuccess() {
        String msg = "GET|/notifications/2/3|{}";
        RequestHandler handler = new RequestHandler(msg, c);
        JSONObject response = new JSONObject(handler.run());
        assertNotNull(response.get("body"));
    }

    /**
     *  fails when typing a user id that is not found
     */
    @Test
    public void GetNotficationFail() {
        String msg = "GET|/notifications/50/3|{}";
        RequestHandler handler = new RequestHandler(msg, c);
        JSONObject response = new JSONObject(handler.run());
        assertEquals(501, Integer.parseInt((String) response.get("status")));
    }
}
