package redis;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

public class Caching {

    private static JedisPool jedisPool = new JedisPool(new JedisPoolConfig(), "localhost",6379);//Integer.parseInt(System.getenv("REDIS_PORT")));

    public static void cacheNotifications(String input,String notifications){
        try (Jedis jedis = jedisPool.getResource()) {
            jedis.set(input, notifications);
        }
    }
    public static String getCachedNotifications(String input) {
        try (Jedis jedis = jedisPool.getResource()) {
            String re = jedis.get(input);
            return re;
        }
    }
    }
