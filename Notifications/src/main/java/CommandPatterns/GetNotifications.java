package CommandPatterns;

import java.rmi.server.ExportException;
import java.sql.*;

import redis.Caching;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;


public class GetNotifications implements Command {

    Notification notification;
    Connection connection;

    /**
     * @param n type notification
     */
    public GetNotifications(Notification n, Connection c) {
        this.notification = n;
        this.connection = c;
    }

    /**
     * @return message
     */
    public String execute() {
        String cachedValue = Caching.getCachedNotifications(notification.notifiedID + "|" + notification.number);
        if (cachedValue != null)
            return cachedValue;
        Statement stmt = null;
        String responseString = "";
        try {
            String sql = "SELECT * FROM _notifications where notified_id=? FETCH FIRST ? ROWS ONLY;";
            stmt = connection.prepareStatement(sql);
            ((PreparedStatement) stmt).setInt(1, notification.notifiedID);
            ((PreparedStatement) stmt).setInt(2, notification.number);
            ResultSet rs = ((PreparedStatement) stmt).executeQuery();
            while (rs.next()) {

                responseString += "{\"id\":\"" + rs.getInt("id") + "\"" + ",\n" +
                        "\"actor_id\":\"" + rs.getString("actor_id") + "\"" + ",\n" +
                        "\"notified_id\":\"" + rs.getString("notified_id") + "\"" + ",\n" +
                        "\"type\":\"" + rs.getString("type") + "\"" + ",\n" +
                        "\"read\":\"" + rs.getString("read") + "\"" + ",\n" +
                        "\"date\":\"" + rs.getString("date") + "\"" + ",\n" + "},\n";
            }
            System.out.println("herere");

            rs.close();
            stmt.close();
            try {
                responseString.substring(0, responseString.length() - 2);
                Caching.cacheNotifications(notification.notifiedID + "|" + notification.number, "{\"body\":[" + responseString + "]\n}");
                return "{\"body\":[" + responseString + "]\n}";
            } catch (StringIndexOutOfBoundsException e) {
                System.out.println(e);
                Caching.cacheNotifications(notification.notifiedID + "|" + notification.number, "{\"status\":\"501\",\"message\":\"Internal Server Error\"}");
                return "{\"status\":\"501\",\"message\":\"Internal Server Error\"}";
            }

        } catch (Exception throwables) {
            throwables.printStackTrace();
            if (throwables instanceof SQLException) {
                Caching.cacheNotifications(notification.notifiedID + "|" + notification.number, "{\"status\":\"501\",\"message\":\"Database error\"}");
                return "{\"status\":\"501\",\"message\":\"Database error\"}";
            } else {
                Caching.cacheNotifications(notification.notifiedID + "|" + notification.number, "{\"status\":\"501\",\"message\":\"Internal Server Error\"}");
                return "{\"status\":\"501\",\"message\":\"Internal Server Error\"}";
            }
        }


    }
}
