package CommandPatterns;
import java.sql.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
public class CreateNotification implements Command {

    Notification notification;
    Connection connection;
    /**
     *
     * @param n type notification
     */
    public CreateNotification(Notification n,Connection c)
    {
        this.notification = n;
        this.connection = c;
    }
    /**
     *
     * @return message
     */
    public String execute()
    {
        Statement stmt = null;
        LocalDate currentDate = LocalDate.now();
        try {
            String sql = "INSERT INTO _notifications (actor_id, notified_id, type, read, date) VALUES (?, ?, ?, 0, ?)";
            stmt = connection.prepareStatement(sql);
            ((PreparedStatement) stmt).setInt(1, notification.actorId);
            ((PreparedStatement) stmt).setInt(2, notification.notifiedID);
            ((PreparedStatement) stmt).setString(3, notification.type);
            ((PreparedStatement) stmt).setString(4, currentDate.toString());
            ((PreparedStatement) stmt).executeUpdate();
            stmt.close();
        } catch (Exception throwables) {
            System.out.println(throwables);
            if(throwables instanceof SQLException)
                return "{\"status\":\"501\",\"message\":\"Database error\"}";
            else
            {
                return "{\"status\":\"501\",\"message\":\"Internal Server Error\"}";
            }
        }
        return "{\"status\":\"201\",\"message\":\"success\"}";
    }
}
