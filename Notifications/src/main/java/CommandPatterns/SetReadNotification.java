package CommandPatterns;

import java.sql.*;

public class SetReadNotification implements Command{

    Notification notification;
    Connection connection;
    /**
     *
     * @param n type notification
     */
    public SetReadNotification(Notification n, Connection c)
    {
        this.notification = n;
        this.connection = c;
    }

    /**
     *
     * @return message
     */
    public String execute(){
        Statement stmt = null;
        try
        {
            String sql = "UPDATE _notifications set read = '1' where id=?;";
            stmt = connection.prepareStatement(sql);
            ((PreparedStatement) stmt).setInt(1, notification.notifiedID);
            ((PreparedStatement) stmt).executeUpdate();
            stmt.close();
            return "{\"status\":\"201\",\"message\":\"success\"}";

        }
        catch (Exception throwables) {
            throwables.printStackTrace();
            if(throwables instanceof SQLException)
            return "{\"status\":\"501\",\"message\":\"Database error\"}";
            else
                {
                    return "{\"status\":\"501\",\"message\":\"Internal Server Error\"}";
                }
        }
    }
}
