package CommandPatterns;

import org.json.JSONException;
import org.json.JSONObject;

public class Notification {

    int id;
    int notifiedID;
    int actorId;
    boolean read;
    String type;
    String text;
    int number;
    boolean err = false;
    String errorMessage;
    int commandtype;

    public int getCommandtype() {
        return commandtype;
    }

    /**
     *
     * @param msg contains method|URI|JSON string
     *            Parses message into id, userId, read, type, text, number, err, errorMessage
     *            all variables need not be set, dependent on the request.
     */
    public Notification(String[] msg)
    {
        JSONObject json;
        if(msg.length > 2)
            json = new JSONObject(msg[2]);
        else
            json = new JSONObject();
        System.out.println(json.toString());
        switch (msg[0])
        {
            case "GET":
                String[] uri = msg[1].split("/");
                if(uri.length == 4)
                {
                    try {
                        notifiedID = Integer.parseInt(uri[2]);
                        number = Integer.parseInt(uri[3]);
                    }
                    catch (Exception e)
                    {
                        System.out.println(e);
                        err = true;
                        errorMessage = "{\"status\": \"404\",\n\"message\": \"user id or number of notifications not found in the uri\"}";
                        return;
                    }
                }
                else{
                    err = true;
                    errorMessage = "{\"status\": \"404\",\n\"message\": \"Invalid URI\"}";
                    return;
                }
                commandtype = 1;
                break;
            case "PUT":
                if(msg[1].equals("/notifications/set-read"))
                {
                    try {
                        id = Integer.parseInt((String) json.get("id"));
                        read = true;
                    }
                    catch (JSONException e)
                    {
                        err = true;
                        errorMessage = "{\"status\": \"404\",\n\"message\": \"Body attributes not found\"}";
                        return;
                    }
                    }
                else{
                    err = true;
                    errorMessage = "{\"status\": \"404\",\n\"message\": \"Invalid URI\"}";
                    return;
                }
                commandtype = 2;
                break;
            case "POST":
                if(msg[1].equals("/notifications"))
                {
                    try {
                        notifiedID = Integer.parseInt((String) json.get("notified_id"));
                        text = (String) json.get("text");
                        type = (String) json.get("type");
                        actorId = Integer.parseInt((String) json.get("actor_id"));
                    }
                    catch (JSONException e)
                    {
                        err = true;
                        errorMessage = "{\"status\": \"404\",\n\"message\": \"Body attributes not found\"}";
                        return;
                    }
                }
                else{
                    err = true;
                    errorMessage = "{\"status\": \"404\",\n\"message\": \"Invalid URI\"}";
                    return;
                }
                commandtype = 3;
                break;
            default:
                err = true;
                errorMessage = "{\"status\": \"404\",\n\"message\": \"Invalid URI\"}";
                return;
        }
    }

    public boolean isRead() {
        return read;
    }

    public String getType() {
        return type;
    }

    public String getText() {
        return text;
    }

    public int getNumber() {
        return number;
    }

    public boolean isErr() {
        return err;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public int getId() {
        return id;
    }

    public int getNotifiedID() {
        return notifiedID;
    }
}
