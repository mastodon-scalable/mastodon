import CommandPatterns.CreateNotification;
import CommandPatterns.GetNotifications;
import CommandPatterns.Notification;
import CommandPatterns.SetReadNotification;

import java.sql.Connection;
import java.util.Arrays;

public class RequestHandler {
    String message;
    final Connection connection;

    public RequestHandler(String msg, Connection c)
    {
        this.message = msg;
        this.connection = c;
    }

    @SuppressWarnings("unchecked")
    public String run()
    {
        String response = "";
        String[] messageSplit = this.message.split("\\|");
        for(String s: messageSplit)
        {
            System.out.println(s);
        }
        System.out.println(Arrays.toString(messageSplit));
        Notification notification = new Notification(messageSplit);
        if(notification.isErr())
        {
            response += notification.getErrorMessage();
        }
        else {
            switch (notification.getCommandtype()) {
                case 1:
                    GetNotifications gn = new GetNotifications(notification, this.connection);response = gn.execute();break;
                case 2:
                    SetReadNotification sn = new SetReadNotification(notification, this.connection);response = sn.execute();break;
                case 3:
                    CreateNotification cn = new CreateNotification(notification, this.connection);response = cn.execute();break;
            }
        }return response;
    }
}
