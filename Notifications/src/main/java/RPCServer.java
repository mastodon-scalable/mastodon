
import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.LongStream;
import java.sql.Connection;
import java.sql.DriverManager;
public class RPCServer {

    private static final String RPC_QUEUE_NAME = "Notifications_queue";


//    public class AsyncCallBack {
    public static long operation(long x) throws Exception {
        if (x < 2) {
            throw new Exception("Negative Range Not Allowed");
        } else {
            return LongStream.range(2, x).map(i -> i * i).reduce(
                    (i, j) -> i + j).getAsLong();
        }
    }
//    }

        /**
     *
     * @param argv  Runs RabbitMQ RPC server and connects to DB
     *              Maps requests to Request handler and waits on response
     */
    public static void main(String[] argv) {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(System.getenv("RABBITMQ_HOST"));
        com.rabbitmq.client.Connection connection = null;
        Connection c = null;
        String DBName = System.getenv("Mastodon_DB_N");
        String Password = System.getenv("Mastodon_DB_Pass");
        String Port = System.getenv("Mastodon_DB_P");
        String Host = System.getenv("Mastodon_DB_H");
        String User = System.getenv("Mastodon_DB_U");
        try {
            Class.forName("org.postgresql.Driver");
            c = DriverManager
                    .getConnection("jdbc:postgresql://"+Host+':'+Port+'/'+DBName,
                    User, Password);//password for db
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println(e.getClass().getName()+": "+e.getMessage());
            System.exit(0);
        }
        System.out.println("Opened database successfully");
        try {
            connection = factory.newConnection();
            final Channel channel = connection.createChannel();

            channel.queueDeclare(RPC_QUEUE_NAME, false, false, false, null);

            channel.basicQos(1);

            System.out.println(" [x] Awaiting RPC requests");

            Connection finalC = c;
            com.rabbitmq.client.DefaultConsumer consumer = new DefaultConsumer(channel) {
                @Override
                public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                    AMQP.BasicProperties replyProps = new AMQP.BasicProperties
                            .Builder()
                            .correlationId(properties.getCorrelationId())
                            .build();
                    System.out.println("Responding to corrID: "+ properties.getCorrelationId());

                    //String response = "";
                    Supplier s = new Supplier() {
                        @Override
                        public Object get() {
                            return "";
                        }
                    };

                    Consumer c = new Consumer() {
                        @Override
                        public void accept(Object o) {
//                                System.out.println("Result: " + o);
                            try {
                                channel.basicPublish("", properties.getReplyTo(), replyProps, ((String)o).getBytes("UTF-8"));
                                channel.basicAck(envelope.getDeliveryTag(), false);
                                // RabbitMq consumer worker thread notifies the RPC server owner thread
                                synchronized (this) {
                                    this.notify();
                                }
                            }
                            catch (Exception e) {
                                System.out.println(e);
                            }
                        }
                    };

                    try {
                        String message = new String(body, "UTF-8");
                        RequestHandler r = new RequestHandler(message, finalC);

                        s = new Supplier() {
                            @Override
                            public Object get() {
                                //long result = 0;
                                String response = "";
                                try {
                                    response += r.run();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                return response;
                            }
                        };

                    } catch (RuntimeException e) {
                        System.out.println(" [.] " + e.toString());
                    }
                    finally {
                        try {
                            CompletableFuture cf = CompletableFuture.supplyAsync(s)
                                    .whenCompleteAsync((res, e) -> {
                                        if (res.getClass() == String.class) {
                                            c.accept(res);
                                        } else {
                                            System.out.println(res.getClass().toString());
                                            ((Exception) res).printStackTrace();
                                        }
                                    });
                            long i =0;
                            while (!cf.isDone()){
                                if(i % 100000000 ==0 && i!=0){
                                    System.out.println("Loops wasted " + i);
                                }
                                i++;
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            };

            channel.basicConsume(RPC_QUEUE_NAME, false, consumer);
            // Wait and be prepared to consume the message from RPC client.
            while (true) {
                synchronized (consumer) {
                    try {
                        consumer.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (IOException | TimeoutException e) {
            e.printStackTrace();
        } finally {
            if (connection != null)
                try {
                    connection.close();
                } catch (IOException _ignore) {
                }
        }
    }
}