--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.10
-- Dumped by pg_dump version 9.6.10

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;


--
-- Name: _users; Type: TABLE; Schema: public; Owner: postgres
--



CREATE TABLE public._users (
    id serial PRIMARY KEY,
    name character varying(18) DEFAULT NULL::character varying,
    handle character varying(10) DEFAULT NULL::character varying,
    password integer,
    email character varying(19) DEFAULT NULL::character varying,
    profile_photo character varying(1) DEFAULT NULL::character varying,
    cover_photo character varying(1) DEFAULT NULL::character varying
);


ALTER TABLE public._users OWNER TO oqvhkqdvuphclp;

--
-- Name: _instances; Type: TABLE; Schema: public; Owner: oqvhkqdvuphclp
--

CREATE TABLE public._instances (
    id serial PRIMARY KEY,
    creator_id serial,
    name character varying(9) DEFAULT NULL::character varying,
    topic character varying(9) DEFAULT NULL::character varying,
    public boolean,
    description character varying(31) DEFAULT NULL::character varying,
    date timestamp,
    FOREIGN KEY (creator_id)
      REFERENCES public._users (id)
);


ALTER TABLE public._instances OWNER TO oqvhkqdvuphclp;

--
-- Name: _blocked; Type: TABLE; Schema: public; Owner: rebasedata
--


CREATE TABLE public._blocked (
    id serial PRIMARY KEY,
    user_id serial,
    blocked_id serial,
    date character varying(19) DEFAULT NULL::character varying,
    FOREIGN KEY (user_id)
      REFERENCES public._users (id),
    FOREIGN KEY (blocked_id)
      REFERENCES public._users (id)
);


ALTER TABLE public._blocked OWNER TO oqvhkqdvuphclp;

--
-- Name: _followers; Type: TABLE; Schema: public; Owner: oqvhkqdvuphclp
--

CREATE TABLE public._followers (
    id serial PRIMARY KEY,
    follower_id serial,
    followed_id serial,
    date character varying(19) DEFAULT NULL::character varying,
    FOREIGN KEY (follower_id)
      REFERENCES public._users (id),
    FOREIGN KEY (followed_id)
      REFERENCES public._users (id)
);


ALTER TABLE public._followers OWNER TO oqvhkqdvuphclp;

--
-- Name: _instance_follow_request; Type: TABLE; Schema: public; Owner: oqvhkqdvuphclp
--

CREATE TABLE public._instance_follow_request (
    id serial PRIMARY KEY,
    user_id serial,
    instance_id serial,
    status character varying(6) DEFAULT NULL::character varying,
    FOREIGN KEY (user_id)
      REFERENCES public._users (id),
    FOREIGN KEY (instance_id)
      REFERENCES public._instances (id)
);


ALTER TABLE public._instance_follow_request OWNER TO oqvhkqdvuphclp;


--
-- Name: _notifications; Type: TABLE; Schema: public; Owner: oqvhkqdvuphclp
--

CREATE TABLE public._notifications (
    id serial PRIMARY KEY,
    actor_id serial,
    notified_id serial,
    type character varying(12) DEFAULT NULL::character varying,
    read smallint,
    date character varying(19) DEFAULT NULL::character varying,
    FOREIGN KEY (actor_id)
      REFERENCES public._users (id),
    FOREIGN KEY (notified_id)
      REFERENCES public._users (id)
);


ALTER TABLE public._notifications OWNER TO oqvhkqdvuphclp;

--
-- Name: _reports; Type: TABLE; Schema: public; Owner: oqvhkqdvuphclp
--

CREATE TABLE public._reports (
    id serial PRIMARY KEY,
    user_id serial,
    reported_id serial,
    text character varying(16) DEFAULT NULL::character varying,
    date character varying(19) DEFAULT NULL::character varying,
    FOREIGN KEY (user_id)
      REFERENCES public._users (id),
    FOREIGN KEY (reported_id)
      REFERENCES public._users (id)
);


ALTER TABLE public._reports OWNER TO oqvhkqdvuphclp;

--
-- Name: _subscribed_instances; Type: TABLE; Schema: public; Owner: oqvhkqdvuphclp
--

CREATE TABLE public._subscribed_instances (
    id serial PRIMARY KEY,
    user_id serial,
    instance_id serial,
    date character varying(19) DEFAULT NULL::character varying,
    FOREIGN KEY (user_id)
      REFERENCES public._users (id),
    FOREIGN KEY (instance_id)
      REFERENCES public._instances (id)
);


ALTER TABLE public._subscribed_instances OWNER TO oqvhkqdvuphclp;

--
-- Data for Name: _users; Type: TABLE DATA; Schema: public; Owner: rebasedata
--

INSERT INTO public._users ( name, handle, password, email, profile_photo, cover_photo) VALUES ('mostafa abdelaziz','mosta',12345,'mosta@mosta.com','','');
INSERT INTO public._users ( name, handle, password, email, profile_photo, cover_photo) VALUES ('mohsen eldemerdash','mohseeeeen',123456,'mohesn@mohsen.com','','');
INSERT INTO public._users ( name, handle, password, email, profile_photo, cover_photo) VALUES ('syaed seka','sekaaa',123456,'seka@seka.com','','');
INSERT INTO public._users ( name, handle, password, email, profile_photo, cover_photo) VALUES ('ahmed hossam','hoss',123456,'hoss@hoos.com','','');
INSERT INTO public._users ( name, handle, password, email, profile_photo, cover_photo) VALUES ('marwan ehab','sengary',123456,'sengary@sengary.com','','');
INSERT INTO public._users ( name, handle, password, email, profile_photo, cover_photo) VALUES ('youssef emad','emad',123456,'emad@emad.com','','');
INSERT INTO public._users ( name, handle, password, email, profile_photo, cover_photo) VALUES ('karim hisham','hisham',123456,'Hisham@hisham.com','','');
INSERT INTO public._users ( name, handle, password, email, profile_photo, cover_photo) VALUES ('ahmed modoo','modoo',123456,'modoo@modoo.com','','');
INSERT INTO public._users ( name, handle, password, email, profile_photo, cover_photo) VALUES ('ahmed hisham','hisham1',1234556,'Hisham@hishma1.com','','');
INSERT INTO public._users ( name, handle, password, email, profile_photo, cover_photo) VALUES ('khaled mohamed','djkhaled',123456,'dj@dj.com','','');

--
-- Data for Name: _instances; Type: TABLE DATA; Schema: public; Owner: oqvhkqdvuphclp
--

INSERT INTO public._instances (creator_id, name, topic, public, description, date) VALUES (1,'twitter','animals',1,'we love animals',2021-04-11);
INSERT INTO public._instances (creator_id, name, topic, public, description, date) VALUES (1,'facebook','science',1,'we love science',2021-04-11);
INSERT INTO public._instances (creator_id, name, topic, public, description, date) VALUES (3,'reddit','opinions',1,'We express our opinions',2021-04-11);
INSERT INTO public._instances (creator_id, name, topic, public, description, date) VALUES (4,'spotify','music',1,'we love music',2021-04-11);
INSERT INTO public._instances (creator_id, name, topic, public, description, date) VALUES (1,'wikihow','how to do',1,'we show you how to do it',2021-04-11);
INSERT INTO public._instances (creator_id, name, topic, public, description, date) VALUES (1,'youtube','videos',1,'we show videos',2021-04-11);
INSERT INTO public._instances (creator_id, name, topic, public, description, date) VALUES (1,'linkedin','jobs',1,'we search for jobs',2021-04-11);
INSERT INTO public._instances (creator_id, name, topic, public, description, date) VALUES (1,'mastodon','instances',1,'we want to make a great project',2021-04-11);
INSERT INTO public._instances (creator_id, name, topic, public, description, date) VALUES (1,'amazon','products',1,'we sell and buy products',2021-04-11);
INSERT INTO public._instances (creator_id, name, topic, public, description, date) VALUES (1,'GUC brain','products',1,'we love machine learning',2021-04-11);
INSERT INTO public._instances (creator_id, name, topic, public, description, date) VALUES (1,'mathisfun','math',0,'we love math',2021-04-11);


--
-- Data for Name: _reports; Type: TABLE DATA; Schema: public; Owner: oqvhkqdvuphclp
--

INSERT INTO public._reports (user_id, reported_id, text, date) VALUES (1,3,'spam',2021-04-11);
INSERT INTO public._reports (user_id, reported_id, text, date) VALUES (2,3,'irrelevant topic',2021-04-11);
INSERT INTO public._reports (user_id, reported_id, text, date) VALUES (5,6,'violence',2021-04-11);
INSERT INTO public._reports (user_id, reported_id, text, date) VALUES (9,7,'irrelevant topic',2021-04-11);
INSERT INTO public._reports (user_id, reported_id, text, date) VALUES (7,8,'spam',2021-04-11);
INSERT INTO public._reports (user_id, reported_id, text, date) VALUES (1,7,'irrelevant topic',2021-04-11);
INSERT INTO public._reports (user_id, reported_id, text, date) VALUES (6,7,'irrelevant topic',2021-04-11);
INSERT INTO public._reports (user_id, reported_id, text, date) VALUES (1,10,'spam',2021-04-11);
INSERT INTO public._reports (user_id, reported_id, text, date) VALUES (1,2,'violence',2021-04-11);


--
-- Data for Name: _blocked; Type: TABLE DATA; Schema: public; Owner: oqvhkqdvuphclp
--
INSERT INTO public._blocked (user_id, blocked_id, date) VALUES (5,6,2021-04-11);
INSERT INTO public._blocked (user_id, blocked_id, date) VALUES (1,2,2021-04-11);
INSERT INTO public._blocked (user_id, blocked_id, date) VALUES (4,2,2021-04-11);
INSERT INTO public._blocked (user_id, blocked_id, date) VALUES (4,9,2021-04-11);
INSERT INTO public._blocked (user_id, blocked_id, date) VALUES (1,8,2021-04-11);
INSERT INTO public._blocked (user_id, blocked_id, date) VALUES (10,3,2021-04-11);
INSERT INTO public._blocked (user_id, blocked_id, date) VALUES (6,4,2021-04-11);


--
-- Data for Name: _followers; Type: TABLE DATA; Schema: public; Owner: oqvhkqdvuphclp
--

INSERT INTO public._followers (follower_id, followed_id, date) VALUES (3,3,2021-04-11);
INSERT INTO public._followers (follower_id, followed_id, date) VALUES (5,7,2021-04-11);
INSERT INTO public._followers (follower_id, followed_id, date) VALUES (9,5,2021-04-11);
INSERT INTO public._followers (follower_id, followed_id, date) VALUES (3,1,2021-04-11);
INSERT INTO public._followers (follower_id, followed_id, date) VALUES (4,6,2021-04-11);
INSERT INTO public._followers (follower_id, followed_id, date) VALUES (2,1,2021-04-11);
INSERT INTO public._followers (follower_id, followed_id, date) VALUES (6,8,2021-04-11);
INSERT INTO public._followers (follower_id, followed_id, date) VALUES (2,7,2021-04-11);
INSERT INTO public._followers (follower_id, followed_id, date) VALUES (10,4,2021-04-11);
INSERT INTO public._followers (follower_id, followed_id, date) VALUES (6,8,2021-04-11);
INSERT INTO public._followers (follower_id, followed_id, date) VALUES (1,2,2021-04-11);


--
-- Data for Name: _instance_follow_request; Type: TABLE DATA; Schema: public; Owner: oqvhkqdvuphclp
--


INSERT INTO public._instance_follow_request (user_id, instance_id, status) VALUES (1,3,'w');
INSERT INTO public._instance_follow_request (user_id, instance_id, status) VALUES (10,4,'w');
INSERT INTO public._instance_follow_request (user_id, instance_id, status) VALUES (8,6,'w');
INSERT INTO public._instance_follow_request (user_id, instance_id, status) VALUES (8,4,'w');
INSERT INTO public._instance_follow_request (user_id, instance_id, status) VALUES (3,4,'w');


--
-- Data for Name: _notifications; Type: TABLE DATA; Schema: public; Owner: oqvhkqdvuphclp
--

INSERT INTO public._notifications (actor_id, notified_id, type, read, date) VALUES (8,1,'subscribtion',0,2021-04-11);
INSERT INTO public._notifications (actor_id, notified_id, type, read, date) VALUES (2,1,'like',0,2021-04-11);
INSERT INTO public._notifications (actor_id, notified_id, type, read, date) VALUES (8,10,'re-toot',0,2021-04-11);
INSERT INTO public._notifications (actor_id, notified_id, type, read, date) VALUES (4,9,'message',0,2021-04-11);
INSERT INTO public._notifications (actor_id, notified_id, type, read, date) VALUES (10,2,'follower',0,2021-04-11);
INSERT INTO public._notifications (actor_id, notified_id, type, read, date) VALUES (10,2,'subscribtion',0,2021-04-11);
INSERT INTO public._notifications (actor_id, notified_id, type, read, date) VALUES (2,4,'message',0,2021-04-11);
INSERT INTO public._notifications (actor_id, notified_id, type, read, date) VALUES (6,8,'like',0,2021-04-11);
INSERT INTO public._notifications (actor_id, notified_id, type, read, date) VALUES (4,8,'subscribtion',0,2021-04-11);
INSERT INTO public._notifications (actor_id, notified_id, type, read, date) VALUES (5,8,'subscribtion',0,2021-04-11);


--
-- Data for Name: _subscribed_instances; Type: TABLE DATA; Schema: public; Owner: rebasedata
--

INSERT INTO public._subscribed_instances (user_id, instance_id, date) VALUES (1,4,2021-04-11);
INSERT INTO public._subscribed_instances (user_id, instance_id, date) VALUES (7,8,2021-04-11);
INSERT INTO public._subscribed_instances (user_id, instance_id, date) VALUES (5,9,2021-04-11);
INSERT INTO public._subscribed_instances (user_id, instance_id, date) VALUES (2,10,2021-04-11);
INSERT INTO public._subscribed_instances (user_id, instance_id, date) VALUES (9,7,2021-04-11);
INSERT INTO public._subscribed_instances (user_id, instance_id, date) VALUES (3,3,2021-04-11);
INSERT INTO public._subscribed_instances (user_id, instance_id, date) VALUES (3,2,2021-04-11);
INSERT INTO public._subscribed_instances (user_id, instance_id, date) VALUES (6,8,2021-04-11);
INSERT INTO public._subscribed_instances (user_id, instance_id, date) VALUES (3,1,2021-04-11);
INSERT INTO public._subscribed_instances (user_id, instance_id, date) VALUES (6,7,2021-04-11);
INSERT INTO public._subscribed_instances (user_id, instance_id, date) VALUES (1,1,2021-04-11);


--
-- oqvhkqdvuphclpQL database dump complete
--

