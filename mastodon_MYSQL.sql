-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: May 13, 2021 at 01:16 AM
-- Server version: 5.7.31
-- PHP Version: 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mastodon`
--

DELIMITER $$
--
-- Procedures
--
DROP PROCEDURE IF EXISTS `accept_subscription_request`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `accept_subscription_request` (IN `request_id` INT, IN `creator_id` INT)  NO SQL
BEGIN 
SET @exist = 0;
SELECT COUNT(*) INTO @exist FROM instances WHERE instances.creator_id = creator_id;
IF @exist = 1 THEN
UPDATE instance_follow_request 
SET status = "accepted" WHERE instance_follow_request.id = request_id;
SET @user_id = 0;
SET @instance_id = 0;
SELECT @user_id:= user_id, @instance_id:= instance_id FROM instance_follow_request WHERE 
instance_follow_request.id = request_id;
SELECT @user_id, @instance_id;
INSERT INTO subscribed_instances(`user_id`, `instance_id`) VALUES(@user_id, @instance_id);
END IF;
END$$

DROP PROCEDURE IF EXISTS `add_cover_photo`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `add_cover_photo` (IN `cover_photo` BLOB, IN `id` INT)  NO SQL
BEGIN
UPDATE users 
SET 
users.cover_photo_photo = cover_photo 
WHERE 
users.id = id; 
END$$

DROP PROCEDURE IF EXISTS `add_description_to_instance`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `add_description_to_instance` (IN `description` VARCHAR(500), IN `instance_id` INT, IN `creator_id` INT)  NO SQL
BEGIN
UPDATE instances
SET description = description
WHERE
instances.id = instance_id AND 
instances.creator_id = creator_id;
END$$

DROP PROCEDURE IF EXISTS `add_profile_photo`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `add_profile_photo` (IN `profile_photo` BLOB, IN `id` INT)  NO SQL
BEGIN 
UPDATE users 
SET
users.profile_photo = profile_photo
WHERE 
users.id = id;
END$$

DROP PROCEDURE IF EXISTS `block_user`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `block_user` (IN `user_id` INT, IN `blocked_id` INT)  NO SQL
BEGIN
INSERT INTO blocked(`user_id`, `blocked_id`)
VALUES(user_id, blocked_id);
END$$

DROP PROCEDURE IF EXISTS `create_instance`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `create_instance` (IN `creator_id` INT, IN `topic` VARCHAR(50), IN `name` VARCHAR(100), IN `public` BOOLEAN, IN `description` VARCHAR(500))  NO SQL
BEGIN
INSERT INTO instances (`creator_id`,`name`, `topic`, `public`, `description`)
VALUES (creator_id, name, topic, public, description);
END$$

DROP PROCEDURE IF EXISTS `create_notification`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `create_notification` (IN `actor` INT, IN `notified` INT, IN `type` ENUM('follower','mention','message','subscription','like','re-toot'))  NO SQL
BEGIN
INSERT INTO notifications (`actor`, `notified`, `type`) VALUES (actro, notified, type);
END$$

DROP PROCEDURE IF EXISTS `delete_account`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_account` (IN `username` VARCHAR(50), IN `password` VARCHAR(50))  NO SQL
BEGIN
DELETE FROM users  
WHERE 
users.name = username AND users.password = password;
END$$

DROP PROCEDURE IF EXISTS `delete_instance`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_instance` (IN `instance_id` INT, IN `creator_id` INT)  NO SQL
BEGIN
DELETE FROM instances 
WHERE 
instances.id = instance_id AND 
instances.creator_id = creator_id;
END$$

DROP PROCEDURE IF EXISTS `deny_subscription_request`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `deny_subscription_request` (IN `request_id` INT, IN `creator_id` INT)  NO SQL
BEGIN 
SET @exist = 0;
SELECT COUNT(*) INTO @exist FROM instances WHERE instances.creator_id = creator_id;
IF @exist = 1 THEN
UPDATE instance_follow_request 
SET status = "rejected" WHERE instance_follow_request.id = request_id;
END IF;
END$$

DROP PROCEDURE IF EXISTS `explore_user_in_instance`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `explore_user_in_instance` (IN `user_id` INT, IN `instance_id` INT)  NO SQL
BEGIN
SELECT * FROM users INNER JOIN instances ON users.instance_id = instances.id WHERE users.id = user_id AND instances.creator_id = creator_id;
END$$

DROP PROCEDURE IF EXISTS `follow_user`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `follow_user` (IN `follower_id` INT, IN `followed_id` INT)  NO SQL
BEGIN
INSERT INTO followers (`follower_id`, `followed_id`) VALUES (follower_id, followed_id);
END$$

DROP PROCEDURE IF EXISTS `instance_recommendation`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `instance_recommendation` (IN `user_id` INT)  NO SQL
BEGIN
SET @topic = "";
SELECT @topic:= topic, COUNT(*) AS magnitude 
FROM instances INNER JOIN subscribed_instances ON instances.id = subscribed_instances.instance_id
WHERE subscribed_instances.user_id = user_id
GROUP BY topic 
ORDER BY magnitude DESC
LIMIT 1;
SELECT * FROM instances WHERE instances.topic = @topic;
END$$

DROP PROCEDURE IF EXISTS `Login`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `Login` (IN `username` VARCHAR(50), IN `password` VARCHAR(50), OUT `valid` INT)  NO SQL
BEGIN
  SELECT count(*) INTO valid
      FROM users u
      WHERE u.name = username && u.password = password;
      
END$$

DROP PROCEDURE IF EXISTS `report_user`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `report_user` (IN `user_id` INT, IN `reported_id` INT, IN `text` ENUM('violence','irrelevant topic','spam','sexual content'))  NO SQL
BEGIN 
INSERT INTO reports(`user_id`, `reported_id`,  `text`) VALUES (user_id, reported_id, text);
END$$

DROP PROCEDURE IF EXISTS `set_instance_private`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `set_instance_private` (IN `instance_id` INT, IN `creator_id` INT)  NO SQL
BEGIN 
UPDATE instances 
SET public = 0 
WHERE 
instances.id = instance_id AND
instances.creator_id = creator_id;
END$$

DROP PROCEDURE IF EXISTS `set_instance_public`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `set_instance_public` (IN `instance_id` INT, IN `creator_id` INT)  NO SQL
BEGIN 
UPDATE instances
SET public = 1
WHERE
instances.id = instance_id AND
instances.creator_id = creator_id;
END$$

DROP PROCEDURE IF EXISTS `sign_up`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sign_up` (IN `instance_id` INT(50), IN `name` VARCHAR(50), IN `handle` VARCHAR(100), IN `password` VARCHAR(50), IN `email` VARCHAR(100), IN `profile_photo` BLOB, IN `cover_photo` BLOB)  NO SQL
BEGIN
INSERT INTO users (`instance_id`, `name`, `handle`, `password`, `email`, `profile_photo`, `cover_photo`) VALUES (instance_id, name, handle, password, email, profile_photo, cover_photo);
END$$

DROP PROCEDURE IF EXISTS `subscribe_to_instance`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `subscribe_to_instance` (IN `instance_id` INT, IN `user_id` INT)  NO SQL
BEGIN
SET @public = 0;
SELECT @public := public FROM instances  
WHERE instances.id = instance_id;
SELECT @public;
IF @public = 1 THEN
INSERT INTO subscribed_instances(`user_id`, `instance_id`) VALUES (user_id, instance_id);
ELSE
INSERT INTO instance_follow_request(`user_id`, `instance_id`) VALUES (user_id, instance_id);
END IF;
END$$

DROP PROCEDURE IF EXISTS `unfollow_user`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `unfollow_user` (IN `follower_id` INT, IN `followed_id` INT)  NO SQL
BEGIN
DELETE FROM followers 
WHERE
followers.follower_id = follower_id AND
followers.followed_id = followed_id;
END$$

DROP PROCEDURE IF EXISTS `update_profile`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `update_profile` (IN `username` VARCHAR(50), IN `password` INT(50), IN `email` VARCHAR(100), IN `id` INT)  NO SQL
BEGIN 
UPDATE users 
SET
users.name = username,
users.password = password,
users.email = email
WHERE
users.id = id;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `blocked`
--

DROP TABLE IF EXISTS `blocked`;
CREATE TABLE IF NOT EXISTS `blocked` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `blocked_id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `blocked_id` (`blocked_id`)
) ENGINE=InnoDB AUTO_INCREMENT=116 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `blocked`
--

INSERT INTO `blocked` (`id`, `user_id`, `blocked_id`, `date`) VALUES
(101, 5, 6, '2021-04-11 04:14:06'),
(102, 1, 2, '2021-04-11 04:14:15'),
(103, 4, 2, '2021-04-11 04:14:24'),
(104, 4, 9, '2021-04-11 04:14:33'),
(105, 1, 8, '2021-04-11 04:15:32'),
(106, 10, 3, '2021-04-11 04:15:42'),
(107, 6, 4, '2021-04-11 04:15:50'),
(108, 6, 4, '2021-05-12 06:02:51'),
(109, 6, 4, '2021-05-12 06:03:15'),
(110, 4, 6, '2021-05-12 06:05:31'),
(115, 9, 11, '2021-05-12 06:09:43');

-- --------------------------------------------------------

--
-- Table structure for table `followers`
--

DROP TABLE IF EXISTS `followers`;
CREATE TABLE IF NOT EXISTS `followers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `follower_id` int(11) NOT NULL,
  `followed_id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `followed_id` (`followed_id`),
  KEY `follower_id` (`follower_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `followers`
--

INSERT INTO `followers` (`id`, `follower_id`, `followed_id`, `date`) VALUES
(1, 3, 3, '2021-04-11 04:12:24'),
(2, 5, 7, '2021-04-11 04:12:31'),
(3, 9, 5, '2021-04-11 04:12:38'),
(4, 3, 1, '2021-04-11 04:12:47'),
(6, 2, 1, '2021-04-11 04:13:05'),
(7, 6, 8, '2021-04-11 04:13:15'),
(8, 2, 7, '2021-04-11 04:13:37'),
(9, 10, 4, '2021-04-11 04:13:45'),
(10, 6, 8, '2021-04-11 04:13:56'),
(11, 1, 2, '2021-04-11 04:26:38'),
(12, 1, 18, '2021-05-12 00:30:25');

-- --------------------------------------------------------

--
-- Table structure for table `instances`
--

DROP TABLE IF EXISTS `instances`;
CREATE TABLE IF NOT EXISTS `instances` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creator_id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_bin NOT NULL,
  `topic` varchar(50) COLLATE utf8_bin NOT NULL,
  `public` tinyint(1) NOT NULL DEFAULT '1',
  `description` varchar(500) COLLATE utf8_bin NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `creator_id` (`creator_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `instances`
--

INSERT INTO `instances` (`id`, `creator_id`, `name`, `topic`, `public`, `description`, `date`) VALUES
(1, 1, 'twitter', 'animals', 1, 'we love animals', '2021-04-11 03:48:14'),
(2, 1, 'facebook', 'science', 1, 'we love science', '2021-04-11 03:49:07'),
(3, 3, 'reddit', 'opinions', 1, 'We express our opinions', '2021-04-11 03:52:44'),
(4, 4, 'spotify', 'music', 1, 'we love music', '2021-04-11 03:53:46'),
(5, 1, 'wikihow', 'how to do', 1, 'we show you how to do it', '2021-04-11 03:54:23'),
(6, 1, 'youtube', 'videos', 1, 'we show videos', '2021-04-11 03:54:49'),
(7, 1, 'linkedin', 'jobs', 1, 'we search for jobs', '2021-04-11 03:55:22'),
(8, 1, 'mastodon', 'instances', 1, 'we want to make a great project', '2021-04-11 03:55:54'),
(9, 1, 'amazon', 'products', 1, 'we sell and buy products', '2021-04-11 03:56:56'),
(10, 1, 'GUC brain', 'products', 1, 'we love machine learning', '2021-04-11 03:58:16'),
(11, 1, 'mathisfun', 'math', 0, 'we love math', '2021-04-11 04:25:08');

-- --------------------------------------------------------

--
-- Table structure for table `instance_follow_request`
--

DROP TABLE IF EXISTS `instance_follow_request`;
CREATE TABLE IF NOT EXISTS `instance_follow_request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `instance_id` int(11) NOT NULL,
  `status` enum('accepted','wating','rejected') COLLATE utf8_bin NOT NULL DEFAULT 'wating',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `instance_id` (`instance_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `instance_follow_request`
--

INSERT INTO `instance_follow_request` (`id`, `user_id`, `instance_id`, `status`) VALUES
(1, 1, 3, 'wating'),
(2, 10, 4, 'wating'),
(3, 8, 6, 'wating'),
(4, 8, 4, 'wating'),
(5, 3, 4, 'wating');

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

DROP TABLE IF EXISTS `notifications`;
CREATE TABLE IF NOT EXISTS `notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `actor_id` int(11) NOT NULL,
  `notified_id` int(11) NOT NULL,
  `type` enum('follower','mention','message','subscribtion','like','re-toot') COLLATE utf8_bin NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `actor_id` (`actor_id`),
  KEY `notified_id` (`notified_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `actor_id`, `notified_id`, `type`, `date`) VALUES
(1, 8, 1, 'subscribtion', '2021-04-11 04:09:58'),
(2, 2, 1, 'like', '2021-04-11 04:10:11'),
(3, 8, 10, 're-toot', '2021-04-11 04:17:35'),
(4, 4, 9, 'message', '2021-04-11 04:17:46'),
(5, 10, 2, 'follower', '2021-04-11 04:17:57'),
(6, 10, 2, 'subscribtion', '2021-04-11 04:18:07'),
(7, 2, 4, 'message', '2021-04-11 04:18:16'),
(8, 6, 8, 'like', '2021-04-11 04:18:26'),
(9, 4, 8, 'subscribtion', '2021-04-11 04:18:37'),
(10, 5, 8, 'subscribtion', '2021-04-11 04:18:47');

-- --------------------------------------------------------

--
-- Table structure for table `reports`
--

DROP TABLE IF EXISTS `reports`;
CREATE TABLE IF NOT EXISTS `reports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `reported_id` int(11) NOT NULL,
  `text` enum('spam','violence','sexual content','irrelevant topic') COLLATE utf8_bin NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `reported_id` (`reported_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `reports`
--

INSERT INTO `reports` (`id`, `user_id`, `reported_id`, `text`, `date`) VALUES
(1, 1, 3, 'spam', '2021-04-11 04:08:51'),
(2, 2, 3, 'irrelevant topic', '2021-04-11 04:09:05'),
(3, 5, 6, 'violence', '2021-04-11 04:09:17'),
(4, 9, 7, 'irrelevant topic', '2021-04-11 04:09:32'),
(5, 7, 8, 'spam', '2021-04-11 04:09:43'),
(6, 1, 7, 'irrelevant topic', '2021-04-11 04:19:01'),
(7, 6, 7, 'irrelevant topic', '2021-04-11 04:19:10'),
(8, 1, 10, 'spam', '2021-04-11 04:19:19'),
(9, 1, 2, 'violence', '2021-04-11 04:23:50'),
(10, 9, 11, 'violence', '2021-05-12 06:18:53');

-- --------------------------------------------------------

--
-- Table structure for table `subscribed_instances`
--

DROP TABLE IF EXISTS `subscribed_instances`;
CREATE TABLE IF NOT EXISTS `subscribed_instances` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `instance_id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `instance_id` (`instance_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `subscribed_instances`
--

INSERT INTO `subscribed_instances` (`id`, `user_id`, `instance_id`, `date`) VALUES
(1, 1, 4, '2021-04-11 04:06:42'),
(2, 7, 8, '2021-04-11 04:06:51'),
(3, 5, 9, '2021-04-11 04:07:01'),
(4, 2, 10, '2021-04-11 04:07:10'),
(5, 9, 7, '2021-04-11 04:07:26'),
(6, 3, 3, '2021-04-11 04:07:40'),
(7, 3, 2, '2021-04-11 04:07:56'),
(8, 6, 8, '2021-04-11 04:08:06'),
(9, 3, 1, '2021-04-11 04:08:15'),
(10, 6, 7, '2021-04-11 04:08:26'),
(11, 1, 1, '2021-04-11 04:26:52');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `instance_id` int(11) DEFAULT NULL,
  `name` varchar(50) COLLATE utf8_bin NOT NULL,
  `handle` varchar(100) COLLATE utf8_bin NOT NULL,
  `password` varchar(50) COLLATE utf8_bin NOT NULL,
  `email` varchar(100) COLLATE utf8_bin NOT NULL,
  `profile_photo` blob,
  `cover_photo` blob,
  PRIMARY KEY (`id`),
  KEY `instance_id` (`instance_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `instance_id`, `name`, `handle`, `password`, `email`, `profile_photo`, `cover_photo`) VALUES
(1, NULL, 'mosta', 'mosta', '123457', 'mosta@tropicana.com', NULL, NULL),
(2, NULL, 'mohsen eldemerdash', 'mohseeeeen', '123456', 'mohesn@mohsen.com', NULL, NULL),
(3, 2, 'syaed seka ', 'sekaaa', '123456', 'seka@seka.com', NULL, NULL),
(4, 1, 'ahmed hossam', 'hoss', '123456', 'hoss@hoos.com', NULL, NULL),
(5, 1, 'marwan ehab', 'sengary', '123456', 'sengary@sengary.com', NULL, NULL),
(6, 10, 'youssef emad', 'emad', '123456', 'emad@emad.com', NULL, NULL),
(7, NULL, 'karim hisham', 'hisham', '123456', 'Hisham@hisham.com', NULL, NULL),
(8, 5, 'ahmed modoo', 'modoo', '123456', 'modoo@modoo.com', NULL, NULL),
(9, 8, 'ahmed hisham', 'hisham1', '1234556', 'Hisham@hishma1.com', NULL, NULL),
(10, 6, 'khaled mohamed', 'dj khaled', '123456', 'dj@dj.com', NULL, NULL),
(11, 1, 'yasser', 'yassora', '12345', 'yaser@yaser.com', NULL, NULL),
(12, 1, 'yasser', 'yassora', '12345', 'yaser@yaser.com', NULL, NULL),
(13, 1, 'yasser', 'yassora', '12345', 'yaser@yaser.com', NULL, NULL),
(14, 1, 'yasser', 'yassora', '12345', 'yaser@yaser.com', NULL, NULL),
(15, 1, 'yasser', 'yassora', '12345', 'yaser@yaser.com', NULL, NULL),
(16, 1, 'sameh', 'sam7', '12345', 'sameh@sameh.com', NULL, NULL),
(17, 1, 'sameh', 'sam7', '12345', 'sameh@sameh.com', NULL, NULL),
(18, 1, 'sameh', 'sam7', '12345', 'sameh@sameh.com', NULL, NULL),
(19, 1, 'sameh', 'sam7', '12345', 'sameh@sameh.com', NULL, NULL),
(20, 1, 'sameh', 'sam7', '12345', 'sameh@sameh.com', NULL, NULL);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `blocked`
--
ALTER TABLE `blocked`
  ADD CONSTRAINT `blocked_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `blocked_ibfk_2` FOREIGN KEY (`blocked_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `followers`
--
ALTER TABLE `followers`
  ADD CONSTRAINT `followers_ibfk_1` FOREIGN KEY (`followed_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `followers_ibfk_2` FOREIGN KEY (`follower_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `instances`
--
ALTER TABLE `instances`
  ADD CONSTRAINT `instances_ibfk_1` FOREIGN KEY (`creator_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `instance_follow_request`
--
ALTER TABLE `instance_follow_request`
  ADD CONSTRAINT `instance_follow_request_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `instance_follow_request_ibfk_2` FOREIGN KEY (`instance_id`) REFERENCES `instances` (`id`);

--
-- Constraints for table `notifications`
--
ALTER TABLE `notifications`
  ADD CONSTRAINT `notifications_ibfk_1` FOREIGN KEY (`actor_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `notifications_ibfk_2` FOREIGN KEY (`notified_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `reports`
--
ALTER TABLE `reports`
  ADD CONSTRAINT `reports_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `reports_ibfk_2` FOREIGN KEY (`reported_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `subscribed_instances`
--
ALTER TABLE `subscribed_instances`
  ADD CONSTRAINT `subscribed_instances_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `subscribed_instances_ibfk_2` FOREIGN KEY (`instance_id`) REFERENCES `instances` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`instance_id`) REFERENCES `instances` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
