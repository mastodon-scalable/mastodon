package util;

import org.json.JSONObject;

public class SubscribeUnsubscribeUtil {
    public static String buildSubscribeInputString(int userId, int instanceId){
        JSONObject object = new JSONObject();
        object.put("userId", userId);
        object.put("instanceId",  instanceId);
        return object.toString();
    }
}
