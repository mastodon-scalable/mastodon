package util;

import com.couchbase.client.core.deps.com.fasterxml.jackson.core.JsonProcessingException;
import com.couchbase.client.core.deps.com.fasterxml.jackson.databind.ObjectMapper;
import models.InstanceModel;
import models.UserModel;
import org.hibernate.Session;
import utility.HibernateUtil;

import java.sql.Timestamp;
import java.util.List;

public class InstanceUtil {
    public static String buildInstanceJson(Integer creatorId, String topic, String desc, String name, Timestamp time, Boolean publik) throws JsonProcessingException {
        InstanceModel inputModel = new InstanceModel(null, creatorId, name, topic, publik, desc, time);
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(inputModel);
    }

    public static int createInstance(Integer creatorId, String topic, String desc, String name, Timestamp time, Boolean publik) {
        return createInstance(creatorId, topic, desc, name, time, publik, null);
    }

    public static int createInstance(Integer creatorId, String topic, String desc, String name, Timestamp time, Boolean publik, List<UserModel> subscribers) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        InstanceModel instanceModel = new InstanceModel(null, creatorId, name, topic, publik, desc, time, subscribers);
        int createdId = (int) session.save(instanceModel);
        session.getTransaction().commit();
        return createdId;
    }

    public static void removeInstance(Integer id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        InstanceModel instanceModel = session.get(InstanceModel.class, id);
        if (instanceModel != null)
            session.remove(instanceModel);
        session.getTransaction().commit();
    }

    public static InstanceModel getInstance(Integer id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        return session.get(InstanceModel.class, id);
    }
}
