package util;

import models.UserModel;
import org.hibernate.Session;
import utility.HibernateUtil;

public class UserUtil {
    public static int createUser(String name, String handle, Integer password, String email, String pPhoto, String cPhoto) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        UserModel userModel = new UserModel(null, name, handle, email, pPhoto, cPhoto);
        int createdId = (int) session.save(userModel);
        session.getTransaction().commit();
        return createdId;
    }

    public static void removeUser(Integer id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        UserModel userModel = session.get(UserModel.class, id);
        if (userModel != null)
            session.remove(userModel);
        session.getTransaction().commit();
    }

    public static UserModel getUser(Integer id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        return session.get(UserModel.class, id);
    }
}
