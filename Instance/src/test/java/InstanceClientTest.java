import com.couchbase.client.core.deps.com.fasterxml.jackson.core.JsonProcessingException;
import commandpatterns.InstanceClient;
import commandpatterns.commands.*;
import org.junit.Test;
import rpc.RPCRequest;
import util.SubscribeUnsubscribeUtil;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class InstanceClientTest {
    @Test
    public void getInstance() throws JsonProcessingException {
        RPCRequest request = new RPCRequest("GET|/instance/12|{}");
        InstanceClient client = new InstanceClient(request);
        assertEquals(client.getCommand().getClass(), GetInstance.class);
    }

    @Test
    public void updateInstance() throws JsonProcessingException {
        RPCRequest request = new RPCRequest("PUT|/instance/12|{}");
        InstanceClient client = new InstanceClient(request);
        assertEquals(client.getCommand().getClass(), UpdateInstance.class);
    }

    @Test
    public void createInstance() throws JsonProcessingException {
        RPCRequest request = new RPCRequest("POST|/instance|{}");
        InstanceClient client = new InstanceClient(request);
        assertEquals(client.getCommand().getClass(), CreateInstance.class);
    }

    @Test
    public void deleteInstance() throws JsonProcessingException {
        RPCRequest request = new RPCRequest("DELETE|/instance/12|{}");
        InstanceClient client = new InstanceClient(request);
        assertEquals(client.getCommand().getClass(), DeleteInstance.class);
    }

    @Test
    public void subscribe() throws JsonProcessingException {
        RPCRequest request = new RPCRequest("POST|/instance/subscribe|"+ SubscribeUnsubscribeUtil.buildSubscribeInputString(0,0));
        InstanceClient client = new InstanceClient(request);
        assertEquals(client.getCommand().getClass(), SubscribeInstance.class);
    }

    @Test
    public void unsubscribe() throws JsonProcessingException {
        RPCRequest request = new RPCRequest("POST|/instance/unsubscribe|"+ SubscribeUnsubscribeUtil.buildSubscribeInputString(0,0));
        InstanceClient client = new InstanceClient(request);
        assertEquals(client.getCommand().getClass(), UnSubscribeInstance.class);
    }

    @Test
    public void invalidMethod() throws JsonProcessingException{
        RPCRequest request = new RPCRequest("INVALID|/instance|{}");
        InstanceClient client = new InstanceClient(request);
        assertTrue(client.isErr());
    }

    @Test
    public void invalidPost() throws JsonProcessingException {
        RPCRequest request = new RPCRequest("POST|/instance/invalid|{}");
        InstanceClient client = new InstanceClient(request);
        assertTrue(client.isErr());
    }
}
