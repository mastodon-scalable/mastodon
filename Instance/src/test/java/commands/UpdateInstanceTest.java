package commands;

import com.couchbase.client.core.deps.com.fasterxml.jackson.core.JsonProcessingException;
import commandpatterns.commands.CreateInstance;
import commandpatterns.commands.UpdateInstance;
import models.InstanceModel;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import util.InstanceUtil;
import util.UserUtil;

import java.sql.Timestamp;
import java.time.Instant;

import static org.junit.Assert.assertEquals;

public class UpdateInstanceTest {


    private static int instanceId;
    private static int testUserId;

    @BeforeClass
    public static void createInstance() {
        testUserId = UserUtil.createUser(
                "testUser",
                "testUser",
                123,
                "test@email.test",
                "",
                ""
        );
        instanceId = InstanceUtil.createInstance(
                testUserId,
                "ttOld",
                "tdOld",
                "testName",
                Timestamp.from(Instant.now()),
                true
        );
    }

    @AfterClass
    public static void removeTestUser() {
        UserUtil.removeUser(testUserId);
        InstanceUtil.removeInstance(instanceId);
    }

    @Test
    public void happyScenario() throws JsonProcessingException {
        try {
            String inputJson = InstanceUtil.buildInstanceJson(
                    null,
                    "ttNew",
                    "tdNew",
                    null,
                    null,
                    null
            );
            UpdateInstance ci = new UpdateInstance(instanceId, inputJson);
            String res = ci.execute();
            assertEquals(UpdateInstance._204_MESSAGE, res);

            InstanceModel updatedInstance = InstanceUtil.getInstance(instanceId);
            assertEquals("ttNew", updatedInstance.getTopic());
            assertEquals("tdNew", updatedInstance.getDescription());
            assertEquals("testName", updatedInstance.getName());
        } finally {
            //teardown
            if (instanceId != 0)
                InstanceUtil.removeInstance(instanceId);
        }
    }

    @Test
    public void malformedInput() throws JsonProcessingException {
        String inputJson = "";
        UpdateInstance ci = new UpdateInstance(instanceId, inputJson);
        String res = ci.execute();
        assertEquals(CreateInstance._400_MESSAGE, res);
    }

    @Test
    public void instanceNotFound() throws JsonProcessingException {
        String inputJson = InstanceUtil.buildInstanceJson(
                null,
                "ttNew",
                "tdNew",
                null,
                null,
                null
        );
        UpdateInstance ci = new UpdateInstance(-1, inputJson);
        String res = ci.execute();
        assertEquals(UpdateInstance._404_MESSAGE, res);
    }

}
