package commands;

import commandpatterns.commands.SubscribeInstance;
import models.InstanceModel;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import util.InstanceUtil;
import util.SubscribeUnsubscribeUtil;
import util.UserUtil;

import java.sql.Timestamp;
import java.time.Instant;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SubscribeTest {
    private int creatorUserId;
    private int instanceId;
    private int subscriberUserId;

    @Before
    public void createInstance() {
        creatorUserId = UserUtil.createUser(
                "testUser",
                "testUser",
                123,
                "test@email.test",
                "",
                ""
        );
        subscriberUserId = UserUtil.createUser(
                "subUser",
                "subUser",
                123,
                "sub@email.test",
                "",
                ""
        );
        instanceId = InstanceUtil.createInstance(
                creatorUserId,
                "ttOld",
                "tdOld",
                "testName",
                Timestamp.from(Instant.now()),
                true
        );
    }

    @Test
    public void happyScenario() {
        String input = SubscribeUnsubscribeUtil.buildSubscribeInputString(subscriberUserId, instanceId);
        SubscribeInstance si = new SubscribeInstance(input);
        String res = si.execute();
        assertEquals(SubscribeInstance._200_MESSAGE, res);
        InstanceModel instance = InstanceUtil.getInstance(instanceId);
        assertTrue(instance.getSubscribers().stream().anyMatch(userModel -> userModel.getId().equals(subscriberUserId)));
    }

    @Test
    public void instanceNotFound(){
        String input = SubscribeUnsubscribeUtil.buildSubscribeInputString(subscriberUserId, -1);
        SubscribeInstance si = new SubscribeInstance(input);
        String res = si.execute();
        assertEquals(SubscribeInstance._404_MESSAGE_INSTANCE, res);
    }

    @Test
    public void userNotFound() {
        String input = SubscribeUnsubscribeUtil.buildSubscribeInputString(-1, instanceId);
        SubscribeInstance si = new SubscribeInstance(input);
        String res = si.execute();
        assertEquals(SubscribeInstance._404_MESSAGE_USER, res);
    }

    @Test
    public void alreadySubscribed(){
        String input = SubscribeUnsubscribeUtil.buildSubscribeInputString(subscriberUserId, instanceId);
        SubscribeInstance si = new SubscribeInstance(input);
        si.execute(); //first time
        String res = si.execute();
        assertEquals(SubscribeInstance._400_MESSAGE, res);
    }

    @After
    public void cleanupInstanceUsers() {
        InstanceUtil.removeInstance(instanceId);
        UserUtil.removeUser(creatorUserId);
        UserUtil.removeUser(subscriberUserId);
    }
}
