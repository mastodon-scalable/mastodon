package commands;

import com.couchbase.client.core.deps.com.fasterxml.jackson.core.JsonProcessingException;
import commandpatterns.commands.DeleteInstance;
import org.junit.*;
import util.InstanceUtil;
import util.UserUtil;

import java.sql.Timestamp;
import java.time.Instant;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class DeleteInstanceTest {


    private int instanceId;
    private static int testUserId;

    @BeforeClass
    public static void createUser() {
        testUserId = UserUtil.createUser(
                "testUser",
                "testUser",
                123,
                "test@email.test",
                "",
                ""
        );
    }

    @Before
    public void createInstance() {
        instanceId = InstanceUtil.createInstance(
                testUserId,
                "ttOld",
                "tdOld",
                "testName",
                Timestamp.from(Instant.now()),
                true
        );
    }

    @Test
    public void happyScenario() {
            DeleteInstance di = new DeleteInstance(instanceId);
            String res = di.execute();

            assertEquals(DeleteInstance._204_MESSAGE, res);
            assertNull(InstanceUtil.getInstance(instanceId));
    }

    @Test
    public void instanceNotFound() throws JsonProcessingException {
        DeleteInstance di = new DeleteInstance(-1);
        String res = di.execute();
        assertEquals(DeleteInstance._404_MESSAGE, res);
    }

    @After
    public void removeInstance() {
        if (InstanceUtil.getInstance(instanceId) != null)
            InstanceUtil.removeInstance(instanceId);
    }

    @AfterClass
    public static void removeTestUser() {
        UserUtil.removeUser(testUserId);
    }

}
