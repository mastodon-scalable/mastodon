package commands;

import com.couchbase.client.core.deps.com.fasterxml.jackson.core.JsonProcessingException;
import commandpatterns.commands.CreateInstance;
import org.json.JSONObject;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import util.InstanceUtil;
import util.UserUtil;

import java.sql.Timestamp;
import java.time.Instant;

import static org.junit.Assert.assertEquals;

public class CreateInstanceTest {

    private static int testUserId;

    @BeforeClass
    public static void createTestUser() {
        testUserId = UserUtil.createUser(
                "testUser",
                "testUser",
                123,
                "test@email.test",
                "",
                ""
        );
    }

    @AfterClass
    public static void removeTestUser() {
        UserUtil.removeUser(testUserId);
    }

    @Test
    public void happyScenario() throws JsonProcessingException {
        int id = -1;
        try {
            String inputJson = InstanceUtil.buildInstanceJson(
                    testUserId,
                    "test",
                    "testDesc",
                    "testName",
                    Timestamp.from(Instant.now()),
                    true
            );
            CreateInstance ci = new CreateInstance(inputJson);
            String res = ci.execute();
            id = new JSONObject(res).getInt("instanceId");
            assertEquals(String.format(CreateInstance._201_MESSAGE, id), res);
        } finally {
            //teardown
            if (id != -1)
                InstanceUtil.removeInstance(id);
        }
    }

    @Test
    public void malformedInput() throws JsonProcessingException {
        String inputJson = "";
        CreateInstance ci = new CreateInstance(inputJson);
        String res = ci.execute();
        assertEquals(CreateInstance._400_MESSAGE, res);
    }

}
