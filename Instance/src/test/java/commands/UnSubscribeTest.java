package commands;

import commandpatterns.commands.UnSubscribeInstance;
import models.InstanceModel;
import models.UserModel;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import util.InstanceUtil;
import util.SubscribeUnsubscribeUtil;
import util.UserUtil;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class UnSubscribeTest {
    private int creatorUserId;
    private int instanceId;
    private int subscriberUserId;

    @Before
    public void createInstance() {
        creatorUserId = UserUtil.createUser(
                "testUser",
                "testUser",
                123,
                "test@email.test",
                "",
                ""
        );
        subscriberUserId = UserUtil.createUser(
                "subUser",
                "subUser",
                123,
                "sub@email.test",
                "",
                ""
        );

        ArrayList<UserModel> list = new ArrayList<>();
        list.add(UserUtil.getUser(subscriberUserId));
        instanceId = InstanceUtil.createInstance(
                creatorUserId,
                "ttOld",
                "tdOld",
                "testName",
                Timestamp.from(Instant.now()),
                true,
                list
        );
    }

    @Test
    public void happyScenario() {
        String input = SubscribeUnsubscribeUtil.buildSubscribeInputString(subscriberUserId, instanceId);
        UnSubscribeInstance usi = new UnSubscribeInstance(input);
        String res = usi.execute();
        assertEquals(UnSubscribeInstance._200_MESSAGE, res);
        InstanceModel instance = InstanceUtil.getInstance(instanceId);
        assertFalse(instance.getSubscribers().stream().anyMatch(userModel -> userModel.getId().equals(subscriberUserId)));
    }

    @Test
    public void instanceNotFound() {
        String input = SubscribeUnsubscribeUtil.buildSubscribeInputString(subscriberUserId, -1);
        UnSubscribeInstance usi = new UnSubscribeInstance(input);
        String res = usi.execute();
        assertEquals(UnSubscribeInstance._404_MESSAGE_INSTANCE, res);
    }

    @Test
    public void userNotFound() {
        String input = SubscribeUnsubscribeUtil.buildSubscribeInputString(-1, instanceId);
        UnSubscribeInstance usi = new UnSubscribeInstance(input);
        String res = usi.execute();
        assertEquals(UnSubscribeInstance._404_MESSAGE_USER, res);
    }

    @Test
    public void notSubscribed() {
        String input = SubscribeUnsubscribeUtil.buildSubscribeInputString(subscriberUserId, instanceId);
        UnSubscribeInstance usi = new UnSubscribeInstance(input);
        usi.execute(); //first time
        String res = usi.execute();
        assertEquals(UnSubscribeInstance._400_MESSAGE, res);
    }

    @After
    public void cleanupInstanceUsers() {
        InstanceUtil.removeInstance(instanceId);
        UserUtil.removeUser(creatorUserId);
        UserUtil.removeUser(subscriberUserId);
    }
}
