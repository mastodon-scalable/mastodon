package commands;

import com.couchbase.client.core.deps.com.fasterxml.jackson.core.JsonProcessingException;
import commandpatterns.commands.GetInstance;
import org.json.JSONObject;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import util.InstanceUtil;
import util.UserUtil;

import java.sql.Timestamp;
import java.time.Instant;

import static org.junit.Assert.assertEquals;

public class GetInstanceTest {

    private static int testUserId;
    private static int instanceId;

    @BeforeClass
    public static void createInstance() {
        testUserId = UserUtil.createUser(
                "testUser",
                "testUser",
                123,
                "test@email.test",
                "",
                ""
        );
        instanceId = InstanceUtil.createInstance(
                testUserId,
                "ttOld",
                "tdOld",
                "testName",
                Timestamp.from(Instant.now()),
                true
        );
    }

    @Test
    public void happyScenario() throws JsonProcessingException {
        GetInstance gi = new GetInstance(instanceId);
        String res = gi.execute();
        String status = new JSONObject(res).getString("status");
        assertEquals("200", status);
    }

    @Test
    public void instanceNotFound() throws JsonProcessingException {
        GetInstance gi = new GetInstance(-1);
        String res = gi.execute();
        assertEquals(GetInstance._404_MESSAGE, res);
    }

    @AfterClass
    public static void removeTestUser() {
        InstanceUtil.removeInstance(instanceId);
        UserUtil.removeUser(testUserId);
    }

}
