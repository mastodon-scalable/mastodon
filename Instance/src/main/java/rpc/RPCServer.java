package rpc;

import com.rabbitmq.client.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeoutException;
import java.util.function.Consumer;
import java.util.function.Supplier;


public class RPCServer {
    private static final String RPC_QUEUE_NAME = "Instance_queue";
    private static final Logger LOG = LoggerFactory.getLogger(RPCServer.class);

    /**
     * @param argv Runs RabbitMQ RPC server and connects to DB
     *             Maps requests to Request handler and waits on response
     */
    public static void main(String[] argv) {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(System.getenv("RABBITMQ_HOST"));
        com.rabbitmq.client.Connection connection = null;

        try {
            connection = factory.newConnection();
            final Channel channel = connection.createChannel();

            channel.queueDeclare(RPC_QUEUE_NAME, false, false, false, null);

            channel.basicQos(1);

            LOG.info(" [x] Awaiting RPC requests");

            com.rabbitmq.client.DefaultConsumer consumer = new DefaultConsumer(channel) {
                @Override
                public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                    AMQP.BasicProperties replyProps = new AMQP.BasicProperties
                            .Builder()
                            .correlationId(properties.getCorrelationId())
                            .build();
                    LOG.info("Responding to corrID: " + properties.getCorrelationId());
                    Consumer c = new Consumer() {
                        @Override
                        public void accept(Object o) {
                            try {
                                channel.basicPublish("", properties.getReplyTo(), replyProps, ((String) o).getBytes("UTF-8"));
                                channel.basicAck(envelope.getDeliveryTag(), false);
                                // RabbitMq consumer worker thread notifies the RPC server owner thread
                                synchronized (this) {
                                    this.notify();
                                }
                            } catch (Exception e) {
                                System.out.println(e);
                            }
                        }
                    };

                    Supplier s = null;

                    try {
                        String message = new String(body, StandardCharsets.UTF_8);
                        RequestHandler r = new RequestHandler(new RPCRequest(message));

                        s = () -> {
                            String response = "";
                            try {
                                response += r.run();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            return response;
                        };

                    } catch (RuntimeException e) {
                        LOG.error(" [.] " + e.toString(), e);
                    } finally {
                        try {
                            CompletableFuture cf = CompletableFuture.supplyAsync(s)
                                    .whenCompleteAsync((res, e) -> {
                                        if (res.getClass() == String.class) {
                                            c.accept(res);
                                        } else {
                                            LOG.debug(res.getClass().toString());
                                            LOG.error(((Exception) res).getMessage());
                                        }
                                    });
                            while (!cf.isDone()) {
                            }
                        } catch (Exception e) {
                            LOG.error(e.getMessage());
                        }
                    }
                }
            };

            channel.basicConsume(RPC_QUEUE_NAME, false, consumer);
            // Wait and be prepared to consume the message from RPC client.
            while (true) {
                synchronized (consumer) {
                    try {
                        consumer.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (IOException | TimeoutException e) {
            e.printStackTrace();
        } finally {
            if (connection != null)
                try {
                    connection.close();
                } catch (IOException _ignore) {
                }
        }
    }
}