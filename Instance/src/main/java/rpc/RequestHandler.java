package rpc;

import com.couchbase.client.core.deps.com.fasterxml.jackson.core.JsonProcessingException;
import commandpatterns.InstanceClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RequestHandler {
    private final RPCRequest request;
    private static final Logger LOG = LoggerFactory.getLogger(RPCServer.class);

    public RequestHandler(RPCRequest request) {
        this.request = request;
    }

    @SuppressWarnings("unchecked")
    public String run() throws JsonProcessingException {
        String response = "";

        LOG.debug("Handling Request: ");
        LOG.debug(request.toString());

        InstanceClient instance = new InstanceClient(request);
        response = instance.isErr() ? instance.getErrorMessage() : instance.getCommand().execute();

        LOG.debug(response);

        return response;
    }
}
