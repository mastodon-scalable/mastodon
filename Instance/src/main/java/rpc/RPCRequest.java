package rpc;

import models.UserModel;

public class RPCRequest {
    private String url;
    private String body;
    private String method;
    private UserModel loggedInUser;

    public RPCRequest(String encoding) {
        String[] split = encoding.split("\\|");
        method = split[0];
        url = split[1];
        body = split[2];
    }

    public String getUrl() {
        return url;
    }

    public String getBody() {
        return body;
    }

    public String getMethod() {
        return method;
    }

    @Override
    public String toString() {
        return "RPCRequest" + this.hashCode() + "{" +
                "url='" + url + '\'' +
                ", body='" + body + '\'' +
                ", method='" + method + '\'' +
                ", loggedInUser=" + loggedInUser +
                '}';
    }
}
