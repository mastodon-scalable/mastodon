package models;

public class UserModel {
    private Integer id;
    private String name;
    private String handle;
    private String email;
    private String profilePhoto;
    private String coverPhoto;

    public UserModel(Integer id, String name, String handle, String email, String profilePhoto, String coverPhoto) {
        this.id = id;
        this.name = name;
        this.handle = handle;
        this.email = email;
        this.profilePhoto = profilePhoto;
        this.coverPhoto = coverPhoto;
    }

    public UserModel() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHandle() {
        return handle;
    }

    public void setHandle(String handle) {
        this.handle = handle;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProfilePhoto() {
        return profilePhoto;
    }

    public void setProfilePhoto(String profilePhoto) {
        this.profilePhoto = profilePhoto;
    }

    public String getCoverPhoto() {
        return coverPhoto;
    }

    public void setCoverPhoto(String coverPhoto) {
        this.coverPhoto = coverPhoto;
    }
}