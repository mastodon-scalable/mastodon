package models;

import java.sql.Timestamp;
import java.util.List;

public class InstanceModel {
    private Integer id;
    private Integer creatorId;
    private String name;
    private String topic;
    private Boolean publik;
    private String description;
    private Timestamp date;
    private List<UserModel> subscribers;

    public InstanceModel() {
    }

    public InstanceModel(Integer id, Integer creatorId, String name, String topic, Boolean publik, String description, Timestamp date, List<UserModel> subscribers) {
        this.id = id;
        this.creatorId = creatorId;
        this.name = name;
        this.topic = topic;
        this.publik = publik;
        this.description = description;
        this.date = date;
        this.subscribers = subscribers;
    }

    public InstanceModel(Integer id, Integer creatorId, String name, String topic, Boolean publik, String description, Timestamp date) {
        this.id = id;
        this.creatorId = creatorId;
        this.name = name;
        this.topic = topic;
        this.publik = publik;
        this.description = description;
        this.date = date;
    }

    public List<UserModel> getSubscribers() {
        return subscribers;
    }

    public void setSubscribers(List<UserModel> subscribers) {
        this.subscribers = subscribers;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(Integer creatorId) {
        this.creatorId = creatorId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public Boolean getPublik() {
        return publik;
    }

    public void setPublik(Boolean publik) {
        this.publik = publik;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public void updateFromInstance(InstanceModel input) {
        if (input.getPublik() != null) setPublik(input.getPublik());
        if (input.getName() != null) setName(input.getName());
        if (input.getDate() != null) setDate(input.getDate());
        if (input.getDescription() != null) setDescription(input.getDescription());
        if (input.getTopic() != null) setTopic(input.getTopic());
    }
}
