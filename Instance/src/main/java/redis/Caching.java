package redis;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

public class Caching {

    private static JedisPool jedisPool = new JedisPool(new JedisPoolConfig(), "localhost",6379);

    public static void cacheInstances(String input,String instances){
        try (Jedis jedis = jedisPool.getResource()) {
            jedis.set(input, instances);
        }
    }
    public static String getCachedInstances(String input) {
        try (Jedis jedis = jedisPool.getResource()) {
            String re = jedis.get(input);
            return re;
        }
    }
}
