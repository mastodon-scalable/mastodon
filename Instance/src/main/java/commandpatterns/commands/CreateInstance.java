package commandpatterns.commands;

import com.couchbase.client.core.deps.com.fasterxml.jackson.core.JsonProcessingException;
import com.couchbase.client.core.deps.com.fasterxml.jackson.databind.ObjectMapper;
import com.couchbase.client.core.deps.com.fasterxml.jackson.databind.exc.MismatchedInputException;
import commandpatterns.Command;
import models.InstanceModel;
import org.hibernate.Session;
import utility.HibernateUtil;

public class CreateInstance implements Command {

    public static final String _201_MESSAGE = "{\"status\":\"201\",\"message\":\"success\", \"instanceId\":%d}";
    public static final String _500_MESSAGE = "{\"status\":\"500\",\"message\":\"Internal Server Error\"}";
    public static final String _400_MESSAGE = "{\"status\":\"400\",\"message\":\"Bad Request\"}";

    private final InstanceModel instanceModel;

    public CreateInstance(String json) throws JsonProcessingException {
        InstanceModel instanceModelTemp = null;
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            instanceModelTemp = objectMapper.readValue(json, InstanceModel.class);
        } catch (MismatchedInputException ignored) {
        }
        instanceModel = instanceModelTemp;
    }

    /**
     * @return message
     */
    public String execute() {
        if (instanceModel == null)
            return _400_MESSAGE;
        int id;
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            id = (int) session.save(instanceModel);
            session.getTransaction().commit();
        } catch (Exception e) {
            return _500_MESSAGE;
        }
        return String.format(_201_MESSAGE, id);
    }
}