package commandpatterns.commands;

import commandpatterns.Command;
import models.InstanceModel;
import models.UserModel;
import org.hibernate.Session;
import org.json.JSONObject;
import utility.HibernateUtil;

public class UnSubscribeInstance implements Command {
    public static final String _400_MESSAGE = "{\"status\":\"400\",\"message\":\"user not subscribed\"}";
    public static final String _500_MESSAGE = "{\"status\":\"500\",\"message\":\"Internal Server Error\"}";
    public static final String _200_MESSAGE = "{\"status\":\"200\",\"message\":\"subscribed\"}";
    private final int userId;
    private final int instanceId;

    public static final String _404_MESSAGE_INSTANCE = "{\"status\":\"404\",\"message\":\"instance not found\"}";
    public static final String _404_MESSAGE_USER = "{\"status\":\"404\",\"message\":\"user not found\"}";


    public UnSubscribeInstance(String inputString) {
        JSONObject input = new JSONObject(inputString);
        userId = input.getInt("userId");
        instanceId = input.getInt("instanceId");
    }

    @Override
    public String execute() {
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();

            InstanceModel instance = session.get(InstanceModel.class, instanceId);
            if (instance == null)
                return _404_MESSAGE_INSTANCE;

            UserModel user = session.get(UserModel.class, userId);
            if (user == null)
                return _404_MESSAGE_USER;

            if (!instance.getSubscribers().contains(user))
                return _400_MESSAGE;

            instance.getSubscribers().remove(user);
            session.getTransaction().commit();
        } catch (Exception e) {
            return _500_MESSAGE;
        }
        return _200_MESSAGE;
    }
}
