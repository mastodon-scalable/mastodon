package commandpatterns.commands;

import com.couchbase.client.core.deps.com.fasterxml.jackson.core.JsonProcessingException;
import com.couchbase.client.core.deps.com.fasterxml.jackson.databind.ObjectMapper;
import com.couchbase.client.core.deps.com.fasterxml.jackson.databind.exc.MismatchedInputException;
import commandpatterns.Command;
import models.InstanceModel;
import org.hibernate.Session;
import utility.HibernateUtil;

public class UpdateInstance implements Command {

    public static final String _204_MESSAGE = "{\"status\":\"204\",\"message\":\"success\"}";
    public static final String _500_MESSAGE = "{\"status\":\"500\",\"message\":\"Internal Server Error\"}";
    public static final String _400_MESSAGE = "{\"status\":\"400\",\"message\":\"Bad Request\"}";
    public static final String _404_MESSAGE = "{\"status\":\"404\",\"message\":\"instance not found\"}";

    private final InstanceModel instanceUpdateModel;

    public UpdateInstance(int id, String body) throws JsonProcessingException {
        InstanceModel instanceUpdateModelTemp = null;
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            instanceUpdateModelTemp = objectMapper.readValue(body, InstanceModel.class);
            instanceUpdateModelTemp.setId(id);
        } catch (MismatchedInputException ignored) {
        }
        instanceUpdateModel = instanceUpdateModelTemp;
    }

    /**
     * @return message
     */
    public String execute() {
        if (instanceUpdateModel == null)
            return _400_MESSAGE;
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            InstanceModel instanceModel = session.get(InstanceModel.class, instanceUpdateModel.getId());
            if (instanceModel == null)
                return _404_MESSAGE;
            instanceModel.updateFromInstance(instanceUpdateModel);
            session.update(instanceModel);
            session.getTransaction().commit();
        } catch (Exception e) {
            return _500_MESSAGE;
        }
        return _204_MESSAGE;
    }
}