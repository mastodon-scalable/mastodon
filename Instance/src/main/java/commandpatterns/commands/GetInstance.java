package commandpatterns.commands;

import com.couchbase.client.core.deps.com.fasterxml.jackson.databind.ObjectMapper;
import commandpatterns.Command;
import models.InstanceModel;
import org.hibernate.Session;
import utility.HibernateUtil;

public class GetInstance implements Command {

    public static final String _404_MESSAGE = "{\"status\":\"404\",\"message\":\"instance not found\"}";
    public static final String _200_MESSAGE = "{\"status\":\"200\",\"message\":\"instance found\", \"data\":%s}";
    public static final String _500_MESSAGE = "{\"status\":\"500\",\"message\":\"Internal Server Error\"}";
    private final int id;

    public GetInstance(int id) {
        this.id = id;
    }

    @Override
    public String execute() {
        ObjectMapper objectMapper = new ObjectMapper();
        InstanceModel instance;
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            instance = session.get(InstanceModel.class, id);
            if (instance == null)
                return _404_MESSAGE;
            return String.format(_200_MESSAGE, objectMapper.writeValueAsString(instance));
        } catch (Exception e) {
            return _500_MESSAGE;
        }
    }
}
