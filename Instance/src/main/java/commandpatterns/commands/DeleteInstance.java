package commandpatterns.commands;

import commandpatterns.Command;
import models.InstanceModel;
import org.hibernate.Session;
import utility.HibernateUtil;

public class DeleteInstance implements Command {

    public static final String _204_MESSAGE = "{\"status\":\"204\",\"message\":\"instance deleted\"}";
    public static final String _404_MESSAGE = "{\"status\":\"404\",\"message\":\"instance not found\"}";
    public static final String _500_MESSAGE = "{\"status\":\"500\",\"message\":\"Internal Server Error\"}";
    private final int id;

    public DeleteInstance(int id) {
        this.id = id;
    }

    @Override
    public String execute() {
        InstanceModel instance;
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            instance = session.get(InstanceModel.class, id);
            if (instance == null)
                return _404_MESSAGE;
            session.remove(instance);
            session.getTransaction().commit();
            return _204_MESSAGE;
        } catch (Exception e) {
            return _500_MESSAGE;
        }
    }
}
