package commandpatterns;

public interface Command {

    public String execute();
}
