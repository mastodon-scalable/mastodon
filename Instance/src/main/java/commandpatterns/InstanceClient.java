package commandpatterns;

import com.couchbase.client.core.deps.com.fasterxml.jackson.core.JsonProcessingException;
import commandpatterns.commands.*;
import rpc.RPCRequest;


public class InstanceClient {

    private boolean err = false;
    private String errorMessage;
    private Command command;

    public InstanceClient(RPCRequest request) throws JsonProcessingException {
        String method = request.getMethod();
        String uri = request.getUrl();
        String body = request.getBody();
        try {
            switch (method) {
                case "GET":
                    String[] uriSplit = uri.split("/");
                    String id = uriSplit[2];
                    command = new GetInstance(Integer.parseInt(id));
                    break;
                case "PUT":
                    uriSplit = uri.split("/");
                    id = uriSplit[2];
                    command = new UpdateInstance(Integer.parseInt(id), body);
                    break;
                case "POST":
                    switch (uri) {
                        case "/instance":
                            command = new CreateInstance(body);
                            break;
                        case "/instance/subscribe":
                            command = new SubscribeInstance(body);
                            break;
                        case "/instance/unsubscribe":
                            command = new UnSubscribeInstance(body);
                            break;
                        default:
                            err = true;
                            errorMessage = "{\"status\": \"404\",\n\"message\": \"Invalid URI\"}";
                            return;
                    }
                    break;
                case "DELETE":
                    uriSplit = uri.split("/");
                    id = uriSplit[2];
                    command = new DeleteInstance(Integer.parseInt(id));
                    break;
                default:
                    err = true;
                    errorMessage = "{\"status\": \"404\",\n\"message\": \"Invalid URI\"}";
            }
        } catch (Exception e) {
            err = true;
            errorMessage = "{\"status\": \"500\",\n\"message\": \"Internal Server Error\"}";
        }
    }

    public Command getCommand() {
        return command;
    }

    public boolean isErr() {
        return err;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}